import React from "react";
import "../../assets/css/sideNav.css";
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookMessenger, faInstagram, faPinterest } from '@fortawesome/free-brands-svg-icons'
import logoQuota from "../../assets/image/LogoQuotatrip.png";
import logo from "../../assets/image/logo.png";

export default function () {
    return (
        <>
            <footer className="w3-padding-32 w3-white w3-center w3-margin-top w3-container">
                <hr />
                <div className="w3-col l4 s12 m4 w3-padding w3-margin-top">
                    <div className="w3-xlarge w3-padding-16 w3-container">
                        <img src={logo} alt="logo" className="foot" />
                    </div>
                </div>
                <div className="w3-col l4 s12 m4 w3-margin-top">
                    <h5 className="color w3-margin-top">Find Us On</h5>
                    <div className="w3-xlarge w3-padding-16 w3-padding color">
                        <a href="https://www.facebook.com/search/top/?q=Holy%20Destination%20Madagascar&epa=SEARCH_BOX" target="_blank" rel="noopener noreferrer" className="mail color"><FontAwesomeIcon icon={faFacebookMessenger} className="w3-margin w3-text-blue" /></a>
                        <FontAwesomeIcon icon={faInstagram} className="w3-margin insta" />
                        <FontAwesomeIcon icon={faPinterest} className="w3-margin w3-text-red" />
                        <FontAwesomeIcon icon={faEnvelope} className="w3-margin w3-text-amber" />
                        <h5 className="w3-margin color">Contact: +261 34 32 255 21</h5>
                        <h5 className="w3-margin color">Mail: <a href={"https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=holydestination@gmail.com"} target="_blank" rel="noopener noreferrer" className="mail color">holydestination@gmail.com</a></h5>
                    </div>
                    <h5><span className="color">Powered by </span><a href="href" target="_blank" className="dev">TheKing</a></h5>
                    <h5><span className="color">Copyright© 2020</span> </h5>
                </div>
                <div className="w3-col l4 s12 m4 w3-padding">
                    <div className="w3-xlarge w3-padding-16 w3-container">
                        <img src={logoQuota} alt="logo" className="foot quota" />
                    </div>
                </div>
            </footer>
        </>
    )
}