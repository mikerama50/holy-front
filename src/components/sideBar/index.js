import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faVideo } from '@fortawesome/free-solid-svg-icons';
import { faMapMarked } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import "../../assets/css/sideNav.css";
import $ from "jquery";

const itineraries = [
    {
        name: "Ambohimanjaka",
        url: "/itinerary/ambohimanjaka"
    },
    {
        name: "Andasibe-Masoala",
        url: "/itinerary/masoala"
    },
    {
        name: "Trekking tour",
        url: "/itinerary/trekking_tour"
    },
    {
        name: "Zafimaniry",
        url: "/itinerary/zafimaniry"
    },
    {
        name: "Southeast",
        url: "/itinerary/southeast"
    },
    {
        name: "North",
        url: "/itinerary/north"
    },
    {
        name: "Andasibe-Palmarium",
        url: "/itinerary/east"
    },
    {
        name: "South",
        url: "/itinerary/south"
    },
    {
        name: "Northwest",
        url: "/itinerary/northwest"
    },
    {
        name: "Southwest",
        url: "/itinerary/southwest"
    },
    {
        name: "Birding tour",
        url: "/itinerary/birding_tour"
    },
    {
        name: "Wildlife Watching",
        url: "/itinerary/wildlife_watching"
    }
]

export default function SideBar(prop) {

    const [open, setOpen] = useState("none");
   
    const closeRightMenu = () => {
        setOpen("none");
        prop.close();
    }

    const toogle = () => {
        $(".list-dropdown").slideToggle();
    }

    useEffect(() => {
        setOpen(prop.open === true ? "block" : "none");
    }, [prop.open]);

    return (
        <>
            <div className="w3-sidebar w3-bar-block w3-card w3-animate-right" style={{ display: open, right: 0, minWidth: 300 }} id="rightMenu">
                <div className="w3-left" style={{ marginBottom: 20 }}>
                    <div onClick={() => closeRightMenu()} style={{ background: "transparent", border: "none", fontSize: 50, marginLeft: 10, marginTop: -10, cursor: "pointer" }}>&times;</div>
                </div>
                <a href="/home/noWelcome" className="link w3-bar-item w3-button"><FontAwesomeIcon icon={faHome} />&nbsp;&nbsp;Home</a>
                <div className="my-dropdown">
                    <div onClick={toogle} className="w3-bar-item w3-button dropdownClick"><FontAwesomeIcon icon={faMapMarked} />&nbsp;&nbsp;Itinerary</div>
                    <div className="list-dropdown">
                        {
                            itineraries.map((item, key) => (
                                <a key={key} className="my-dropdown-list" href={item.url}>{item.name}</a>
                            ))
                        }

                    </div>
                </div>
                <a href="/video" className="link w3-bar-item w3-button"><FontAwesomeIcon icon={faVideo} />&nbsp;&nbsp;Videos</a>
                <a href="/experience_warranty" className="link w3-bar-item w3-button"><FontAwesomeIcon icon={faCheckCircle} />&nbsp;&nbsp;Warranty</a>
            </div>
        </>
    )
}