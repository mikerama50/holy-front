import React, {useEffect } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import $ from "jquery";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faMapMarked, faCheckCircle, faPhoneAlt,faCaretRight, faVideo } from '@fortawesome/free-solid-svg-icons';
import logo from "../../assets/image/logo.png";
import "../../assets/css/navbar.css";
import { Popover, Whisper} from 'rsuite';

const itineraries = [
    {
        name: "Ambohimanjaka",
        url: "/itinerary/ambohimanjaka"
    },
    {
        name: "Andasibe-Masoala",
        url: "/itinerary/masoala"
    },
    {
        name: "Trekking tour",
        url: "/itinerary/trekking_tour"
    },
    {
        name: "Zafimaniry",
        url: "/itinerary/zafimaniry"
    },
    {
        name: "Southeast",
        url: "/itinerary/southeast"
    },
    {
        name: "North",
        url: "/itinerary/north"
    },
    {
        name: "Andasibe-Palmarium",
        url: "/itinerary/east"
    },
    {
        name: "South",
        url: "/itinerary/south"
    },
    {
        name: "Northwest",
        url: "/itinerary/northwest"
    },
    {
        name: "Southwest",
        url: "/itinerary/southwest"
    },
    {
        name: "Birding tour",
        url: "/itinerary/birding_tour"
    },
    {
        name: "Wildlife Watching",
        url: "/itinerary/wildlife_watching"
    }
]

const Speaker = ({ content, ...props }) => {
    return (
        <Popover {...props}>
            {
                itineraries.map((item, key) => (
                    <div key={key}>
                        <a href={item.url} className="it w3-text-grey w3-large w3-hover-text-red w3-round-large w3-hover-light-grey w3-block w3-padding">{item.name}</a>
                    </div>
                ))
            }
        </Popover>
    );
};

const MyNavbar = (prop) => {

    useEffect(() => {

        $(".logo").css({
            "width": "100px",
            "height": "auto",
            "transition": "0.5s"
        });

        $(document).ready(() => {

            responsive();
            $(window).resize(() => {
                responsive();
            });

            function responsive() {
                var windowWidth = $(window).width();
                $("#test").html(windowWidth);
            }
        });
        const updateNavbarColor = () => {


            var scrollVal = document.documentElement.scrollTop;
            if (scrollVal === 0) {
                $(".my-nav").css({
                    "height": "90px",
                    "transition": "0.5s",
                    "box-shadow": "none",
                    "z-index": "0"
                });
                $(".logo").css({
                    "width": "100px",
                    "height": "auto",
                    "transition": "0.5s"
                });
            } else {
                $(".my-nav").css({
                    "height": "70px",
                    "transition": "0.5s",
                    "box-shadow": "1px 1px 5px rgba(0, 0, 0, 0.2)",
                    "z-index": "100"
                });
                $(".logo").css({
                    "width": "60px",
                    "height": "auto",
                    "transition": "0.5s"
                });

            }
        };

        window.addEventListener("scroll", updateNavbarColor);

    }, []);

    return (
        <Navbar light expand="md" navbar="true" className="my-nav myNav">

            <NavbarBrand href="/home/noWelcome">
                {/* <div id="test" style={{ color: "black" }}></div> */}
                <img src={logo} alt="logo" className="logo" />
            </NavbarBrand>
            <NavbarToggler onClick={prop.sidebarOpen} />
            <Collapse navbar >
                <Nav className="ml-auto myNavContainer" navbar>
                    <NavItem className="myNavItem">
                        <NavLink href="/home/noWelcome" className="myNavLink w3-text-teal w3-hover-text-red"><FontAwesomeIcon icon={faHome} />&nbsp;Home</NavLink>
                    </NavItem>
                    <NavItem className="myNavItem">

                        <Whisper
                            trigger="hover"
                            placement="bottom"
                            speaker={<Speaker content={`I am positioned to the`} />}
                            enterable
                        >
                            <NavLink href="#" id="itinerary" appearance="subtle" className="myNavLink w3-text-teal w3-hover-text-red"><FontAwesomeIcon icon={faMapMarked} />&nbsp;Itinerary&nbsp;<FontAwesomeIcon icon={faCaretRight} className="arrow"/></NavLink>
                        </Whisper>
                    </NavItem>
                    <NavItem className="myNavItem">
                        <NavLink href="/video" className="myNavLink w3-text-teal w3-hover-text-red"><FontAwesomeIcon icon={faVideo} />&nbsp;Videos</NavLink>
                    </NavItem>
                    <NavItem className="myNavItem">
                        <NavLink href="/experience_warranty" className="myNavLink w3-text-teal w3-hover-text-red"><FontAwesomeIcon icon={faCheckCircle} />&nbsp;Warranty</NavLink>
                    </NavItem>
                    <NavItem className="contactNavLink" >
                        <span className="contact w3-text-red"><FontAwesomeIcon icon={faPhoneAlt} />&nbsp;+261 34 32 255 21</span>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    );
}

export default MyNavbar;