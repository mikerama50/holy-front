import React, { useState, useEffect } from 'react';
import './assets/css/App.css';
import './assets/css/w3.css';
import './assets/css/font-face.css';
import './assets/css/w3.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'rsuite/dist/styles/rsuite-default.css';
import Content from "./router_manager";
import MyNavBar from "./components/navbar";
import SideNav from "./components/sideBar";
import Footer from "./components/footer";
import $ from "jquery";

function App() {

	const [openSideNav, setOpenSidenav] = useState(false);

	const openSideNavFromNavBar = () => {
		setOpenSidenav(!openSideNav);
	}

	const closeSideNav = () => {
		setOpenSidenav(false);
	}

	useEffect(() => {
		$(document).ready(() => {
			$(window).resize(() => {
				setOpenSidenav(false);
			});
		});
	});

	return (
		<>
			<div className="sideNavContainer">
				<SideNav open={openSideNav} close={closeSideNav} />
			</div>
			<header className="myheader" style={{ marginBottom: 90 }}>
				<MyNavBar sidebarOpen={openSideNavFromNavBar} />
			</header>
			<section className="mysection">
				<Content />
			</section>
			<footer className="myfooter">
				<Footer/>
			</footer>
		</>
	);
}

export default App;
