const data = [
    {
        name: "Lemurs",
        id:"lemur",
        description: "Highlight species in Madagascar",
        image: "/image/gallery2/lemur.jpg"
    },
    {
        id:"tortoise",
        name: "Tortoises",
        description: "Endemic species in Madagascar",
        image: "/image/gallery2/turtle.jpg"
    },
    {
        id:"geological",
        name: "Geological features",
        description: "An iconic rock formation of Madagascar",
        image: "/image/gallery2/tsingy.jpg"
    },
    {
        id:"cultural",
        name: "Various interesting aspect cultural and people",
        description: "",
        image: "/image/gallery2/zafimaniry.jpg"
    },
    {
        id:"bird",
        name: "Birds",
        description: "Unique avifauna of Madagascar",
        image: "/image/gallery2/bird.jpg"
    },
    {
        id:"flora",
        name: "Flora of Madagascar",
        description: "",
        image: "/image/gallery2/baobab.jpg"
    },
    {
        id:"handicraft",
        name: "Handicraft",
        description: "Malagasy woodcarving style",
        image: "/image/gallery2/handicraft.jpg"
    },
    {
        id:"chameleon",
        name: "Chameleons",
        description: "An outstanding species of Madagascar",
        image: "/image/gallery2/chameleon.jpg"
    },
    {
        id:"amphibian",
        name: "Malagasy Amphibians",
        description: "Unique species richness",
        image: "/image/gallery2/anphibian.jpg"
    },
    {
        id:"reptile",
        name: "Reptiles",
        description: "Reptiles of Madagascar",
        image: "/image/gallery2/reptile.jpg"
    },
    {
        id:"landscape",
        name: "Landscape",
        description: "",
        image: "/image/gallery2/landscape.jpg"
    },
    {
        id:"insect",
        name: "Malagasy insects",
        description: "",
        image: "/image/gallery2/insect.jpg"
    }
];

export default data;