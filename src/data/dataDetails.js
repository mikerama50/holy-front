const data = {
    lemur: {
        favori: "/image/lemurs/favori.jpg",
        description: "Lemurs are the star of Madagascar because they are not found anywhere else in the world. They belong to a group of primates called prosimians which means “before the monkeys.” Lemurs play a significant role in the pollination of certain plant species in the Malagasy forest and also act as an important seeds dispersal agent. Lemurs occur in different kind of habitat in Madagascar: spiny forest, dry deciduous forest, evergreen forest, littoral and gallery. Indri indri is the largest living species of lemur and is restricted to lowland and mid altitude tropical rainforest, mouse lemurs are the most abundant species, as they are more adaptable and colonizing different kinds of habitat including degraded forest, even plantation area.",
        imagesByLocalisation: [
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Common Brown Lemur",
                        image: "/image/lemurs/lemur7.jpg",
                        video: "https://www.youtube.com/embed/Cjdnz9eRWF0",
                        loc: "Akanin'ny Nofy",
                    },
                    {
                        name: "Black lemur female",
                        image: "/image/lemurs/lemur6.jpg",
                        video: "https://www.youtube.com/embed/ykzoPCK3d0c",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Aye-Aye",
                        image: "/image/lemurs/lemur28.jpg",
                        video: "",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Crowned Lemur",
                        image: "/image/lemurs/lemur8.jpg",
                        video: "https://www.youtube.com/embed/At6AsueoZWg",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Common Brown Lemur",
                        image: "/image/lemurs/lemur9.jpg",
                        video: "https://www.youtube.com/embed/nQBP_H2Ijms",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Black lemur female",
                        image: "/image/lemurs/lemur10.jpg",
                        video: "",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Ruffed lemur",
                        image: "/image/lemurs/lemur11.jpg",
                        video: "https://www.youtube.com/embed/dMP0OiDm9kg",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Indri Babakoto",
                        image: "/image/lemurs/lemur12.jpg",
                        video: "https://www.youtube.com/embed/0odRAbynP-o",
                        loc: "Andasibe"
                    },
                    {
                        name: "Bamboo lemur",
                        image: "/image/lemurs/lemur23.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Weasel sportive lemur",
                        image: "/image/lemurs/lemur25.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Mouse lemur",
                        image: "/image/lemurs/lemur31.jpg",
                        video: "https://www.youtube.com/embed/xzBq7sU6me0",
                        loc: "Andasibe"
                    },
                    {
                        name: "Diademed sifaka",
                        image: "/image/lemurs/lemur32.jpg",
                        video: "",
                        loc: "Andasibe"
                    }
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur1.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur2.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur3.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur4.jpg",
                        video: "https://www.youtube.com/embed/3AbeA9nZ2X8",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        name: "Bamboo lemur",
                        image: "/image/lemurs/lemur33.jpg",
                        video: "https://www.youtube.com/embed/sPHHnqN5rKc",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur27.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur58.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur59.jpg",
                        video: "https://www.youtube.com/embed/Uj7vu-kbBxU",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur60.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur61.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur62.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur63.jpg",
                        video: "https://www.youtube.com/embed/8_6oZE7h4Xw",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur64.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Golden Bamboo lemur",
                        image: "/image/lemurs/lemur67.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur68.jpg",
                        video: "https://www.youtube.com/embed/oArAm00IrNE",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur69.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur70.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur71.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur72.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur73.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur75.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur76.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur77.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur78.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur79.jpg",
                        video: "https://www.youtube.com/embed/YSrNs48KQkY",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "Red bellied lemur",
                        image: "/image/lemurs/lemur80.jpg",
                        video: "",
                        loc: "Ranomafana National Park"
                    },
                    {
                        name: "",
                        image: "/image/plaque/plaqueF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF1.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF2.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF3.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF6.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF7.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF7.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF8.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF12.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF14.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF15.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF16.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF17.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF18.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF19.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF20.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF21.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF22.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF23.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF24.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF25.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF26.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF27.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF28.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF29.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF30.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF31.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF32.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF33.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF34.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF35.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF36.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF37.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF38.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/lemurs/lemurF39.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    }
                ]
            },
            {
                localisation: "Mahajanga",
                species: [
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur38.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur39.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur40.jpg",
                        video: "https://www.youtube.com/embed/FswG32SoFgM",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur41.jpg",
                        loc: "Ankarafantsika",
                        video: ""
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur42.jpg",
                        loc: "Ankarafantsika",
                        video: ""
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur43.jpg",
                        loc: "Ankarafantsika",
                        video: ""
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur44.jpg",
                        loc: "Ankarafantsika",
                        video: "https://www.youtube.com/embed/Hec7NhXmc-Y"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur45.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Microcebus ravelobensis",
                        image: "/image/lemurs/lemur46.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur47.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Microcebus ravelobensis",
                        image: "/image/lemurs/lemur48.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Mongoose lemur",
                        image: "/image/lemurs/lemur51.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur52.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Common brown lemur",
                        image: "/image/lemurs/lemur53.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur54.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur55.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Coquerel's sifaka",
                        image: "/image/lemurs/lemur56.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Common brown lemur",
                        image: "/image/lemurs/lemur57.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    }
                ]
            },
            {
                localisation: "Tulear",
                species: [
                    {
                        name: "Verreaux's Sifaka",
                        image: "/image/lemurs/lemur34.jpg",
                        video: "https://www.youtube.com/embed/5Wf4qNlnBm4",
                        loc: "Zombitse National Park"

                    },
                    {
                        name: "Hubbard's sportive lemur",
                        image: "/image/lemurs/lemur36.jpg",
                        video: "https://www.youtube.com/embed/RO6uF6D_Oc0",
                        loc: "Zombitse National Park"
                    },
                    {
                        name: "Verreaux's Sifaka",
                        image: "/image/lemurs/lemur35.jpg",
                        video: "",
                        loc: "Zombitse National Park"
                    }
                ]
            },
            {
                localisation: "Manakara",
                species: [
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur116.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur117.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur13.jpg",
                        video: "https://www.youtube.com/embed/V_W1OeOFun0",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur14.jpg",
                        video: "https://www.youtube.com/embed/QdyGpBebEN4",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur16.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur17.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur18.jpg",
                        video: "https://www.youtube.com/embed/kC-YXyHFN1k",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur19.jpg",
                        loc: "Heden Cidy",
                        video: "",
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur20.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur21.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur22.jpg",
                        video: "https://www.youtube.com/embed/FcNw9by6PyA",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur81.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur82.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur83.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur84.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur85.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur86.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur87.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur88.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur89.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur90.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur91.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur92.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Red-fronted Lemur",
                        image: "/image/lemurs/lemur93.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur94.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur95.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Ring-tailed lemur",
                        image: "/image/lemurs/lemur96.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur97.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur98.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur99.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur100.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur101.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur102.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur103.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur104.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur105.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur106.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur107.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur108.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur109.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur110.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur111.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur112.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur113.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur114.jpg",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "",
                        image: "/image/lemurs/lemur115.jpg",
                        loc: "Heden Cidy"
                    }

                ]
            }
        ],
    },

    tortoise: {
        favori: "/image/turtles/favori.jpg",
        description: "Madagascar is home of the rarest tortoise in the world, radiated tortoise is one of the six endemic which is found in the semi-arid region of southern of Madagascar. This species is locally endemic to the spiny forest and is one of the most spectacular of the land tortoise species. The ongoing degradation of their habitat and international pet trade reported to be in decline the population number in their natural habitat.",
        imagesByLocalisation: [
            {
                localisation: "Tulear",
                species: [
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle1.jpg",
                        video: "",
                        loc: "Village de Tortue"
                    },
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle4.jpg",
                        video: "",
                        loc: "Village de Tortue"
                    },
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle6.jpg",
                        video: "",
                        loc: "Village de Tortue"
                    },
                ]
            },
            {
                localisation: "Manakara",
                species: [
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle2.jpg",
                        video: "https://www.youtube.com/embed/XdSD1LA2llY",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle3.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle5.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle7.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    },
                    {
                        name: "Radiated tortoise",
                        image: "/image/turtles/turtle8.jpg",
                        video: "",
                        loc: "Heden Cidy"
                    }
                ]
            }
        ]
    },

    bird: {
        favori: "/image/bird/favori.jpg",
        description: "Madagascar has a poor bird’s community compared to the African mainland and the next door Islands. What makes Madagascar different is its high rate of endemism, not only the species level but the families as well, such as Vangas-couas- asity-Mesites… Most of the birds are forest dwelling species, whereas some considerable species are ground dwelling one. ",
        imagesByLocalisation: [
            {
                localisation: "Tulear",
                species: [
                    {
                        name: "Subdesert mesite",
                        image: "/image/bird/bird6.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Long-tailed gound roller",
                        video: "",
                        image: "/image/bird/bird7.jpg",
                        loc: "Ifaty"
                    },
                    {
                        name: "Chabert Vanga",
                        image: "/image/bird/bird4.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "madagascar Hoopoe",
                        image: "/image/bird/bird2.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Soimanga sunbird",
                        image: "/image/bird/bird3.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Red-tailed Vanga",
                        image: "/image/bird/bird10.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Madagascar bee-eaters",
                        image: "/image/bird/bird8.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Running Coua",
                        image: "/image/bird/bird5.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Common Newtonia",
                        image: "/image/bird/bird17.jpg",
                        video: "",
                        loc: "Ifaty"
                    }
                ]
            },
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Nuthatch Vanga",
                        image: "/image/bird/bird11.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Blue Coua",
                        image: "/image/bird/bird12.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Red fronted Coua",
                        image: "/image/bird/bird14.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Scaly ground roller",
                        image: "/image/bird/bird16.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Ward's flycatcher",
                        image: "/image/bird/bird36.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Spectacle Tretraka",
                        image: "/image/bird/bird39.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Paradise flycather",
                        image: "/image/bird/bird37.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Collard nightjar",
                        image: "/image/bird/bird9.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Sunbird asity",
                        image: "/image/bird/bird13.jpg",
                        video: "",
                        loc: "Andasibe"
                    }
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "Madagascar bee eater",
                        image: "/image/bird/bird8.jpg",
                        video: "",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "Madagacar white eye",
                        image: "/image/bird/bird20.jpg",
                        video: "",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "Crested drongo",
                        image: "/image/bird/bird29.jpg",
                        video: "",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "Brown mesite",
                        image: "/image/bird/bird40.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        name: "Greater vasa parrot",
                        image: "/image/bird/bird35.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        name: "Madagascar Stonechat",
                        image: "/image/bird/bird18.jpg",
                        video: "",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "Madagacar kestrel",
                        image: "/image/bird/bird19.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Madagascar bulbul",
                        image: "/image/bird/bird26.jpg",
                        video: "",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "Benson's Rock trush",
                        image: "/image/bird/bird1.jpg",
                        video: "",
                        loc: "Isalo"
                    }
                ]
            },
            {
                localisation: "Antananarivo",
                species: [
                    {
                        name: "Knob-billed duck",
                        image: "/image/bird/bird30.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "White faced duck",
                        image: "/image/bird/bird32.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "Red billed teal",
                        image: "/image/bird/bird31.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "Black crowened night heron",
                        video: "",
                        loc: "Tsarasaotra Park",
                        image: "/image/bird/bird33.jpg",
                    },
                    {
                        name: "Squacco Heron",
                        image: "/image/bird/bird42.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "Madagascar wagtail",
                        image: "/image/bird/bird46.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "Madagacar bushlark",
                        image: "/image/bird/bird47.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "Malagasy kingfisher",
                        image: "/image/bird/bird41.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    },
                    {
                        name: "Madagascar nightjar",
                        image: "/image/bird/bird24.jpg",
                        video: "",
                        loc: "Tsarasaotra Park"
                    }
                ]
            },
            {
                localisation: "Mahajanga",
                species: [
                    {
                        name: "Sakalava weaver",
                        image: "/image/bird/bird25.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Madagascar magpie-robin",
                        image: "/image/bird/bird23.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Yellow billed kite",
                        image: "/image/bird/bird45.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Little grebe",
                        image: "/image/bird/bird44.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Namaqua dove",
                        image: "/image/bird/bird28.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Pied crow",
                        image: "/image/bird/bird48.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Great Egret",
                        image: "/image/bird/bird49.jpg",
                        video: "https://www.youtube.com/embed/A6FTjwWb0jw",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "",
                        image: "/image/bird/bird50.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "",
                        image: "/image/bird/bird51.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Darter",
                        image: "/image/bird/bird52.jpg",
                        video: "https://www.youtube.com/embed/mqZiJV3-bOo",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Madagascar king fisher",
                        image: "/image/bird/bird53.jpg",
                        video: "https://www.youtube.com/embed/FeR8JrGblX4",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "",
                        image: "/image/bird/bird54.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "",
                        image: "/image/bird/bird55.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "White faced duck",
                        image: "/image/bird/bird56.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Magpie Robin",
                        image: "/image/bird/bird57.jpg",
                        video: "https://www.youtube.com/embed/CdFYdQes6Ns",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Ardea Humblot's Heron",
                        image: "/image/bird/bird58.jpg",
                        video: "https://www.youtube.com/embed/Vef4PuCwjC8",
                        loc: "Ankarafantsika"
                    }
                ]
            }
        ]
    },

    cultural: {
        favori: "/image/zafimaniry/favori.jpg",
        description: "Malagasy people have a rich cultural heritage from their Asian, Malayo-polynesian, African and even European origins. Those various influences have blended in different ways and creating regional cultural diversity. Their peaceful and harmonious society are characterized by the traditional tremendous value \"Fihavanana\" a Malagasy term for solidarity and it remains one of the most treasured and practice cultural values in Madagascar to this day. The commonest  which promoting this important cultural value is “Kabary” is an art form  and is a harmonious and strictly structured speech , practiced by different ethnic group with their own different dialects.",
        //description: "Zafimaniry is a small ethnic group located in the eastern of Madagascar which has been recognized by UNESCO and recorded in the immaterial heritage of humanity. Not only that, the Zafimaniry ethnic group present a real originality by their carved and engraved, well realized in a block of solid wood, with great luxury of patterns.  Zafimaniry is also an interesting area for the trekkers, which of their traditional houses are very fascinating and one of the most luxuriant architectures in the island. This amazing creature composed exclusively wood-based; closely resemble the features of the old wooden house of Betsileo and Imerina. The village Zafimaniry is consequently in a way an existing museum whose wood art.",
        imagesByLocalisation: [
            {
                localisation: "Ambositra",
                species: [
                    {
                        name: "Zafimaniry Traditional village",
                        image: "/image/zafimaniry/zafimaniry1.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Typical Zafimaniry House",
                        image: "/image/zafimaniry/zafimaniry2.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Zafimaniry hair style",
                        image: "/image/zafimaniry/zafimaniry3.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        image: "/image/zafimaniry/zafimaniry4.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        image: "/image/zafimaniry/zafimaniry6.jpg",
                        video: "https://www.youtube.com/embed/fchwuY-BIkU",
                        loc: "Antoetra"
                    },
                    {
                        name: "Art of Woodcarving",
                        image: "/image/zafimaniry/zafimaniry17.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Water fall",
                        image: "/image/zafimaniry/zafimaniry8.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Water fall",
                        image: "/image/zafimaniry/zafimaniry9.jpg",
                        video: "https://www.youtube.com/embed/7Av9P8EtFYY",
                        loc: "Antoetra"
                    },
                    {
                        name: "Basic house tools",
                        image: "/image/zafimaniry/zafimaniry10.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Typical Zafimaniry women",
                        image: "/image/zafimaniry/zafimaniry11.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Traditional hat",
                        image: "/image/zafimaniry/zafimaniry12.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Zafimaniry Traditional village",
                        image: "/image/zafimaniry/zafimaniry13.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Typical Zafimaniry House",
                        image: "/image/zafimaniry/zafimaniry14.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "Roof evolution",
                        image: "/image/zafimaniry/zafimaniry15.jpg",
                        video: "",
                        loc: "Antoetra"
                    },
                    {
                        name: "A newly born Zafimaniry baby",
                        image: "/image/zafimaniry/zafimaniry16.jpg",
                        video: "",
                        loc: "Antoetra"
                    }
                ]
            },
            {
                localisation: "Antananarivo",
                species: [
                    {
                        name: "Ambohimanga landscape",
                        image: "/image/cultural/cultural1.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "Ambohimanga queen palace",
                        image: "/image/cultural/cultural3.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "Ambohimanga queen palace gate",
                        image: "/image/cultural/cultural4.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural5.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "Ambohimanga (blue mount)",
                        image: "/image/cultural/cultural6.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "Ambohidratrimo Royal palace",
                        image: "/image/cultural/cultural7.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural8.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "Ambohimanga Traditional wall",
                        image: "/image/cultural/cultural9.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural10.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "Traditional popular game",
                        image: "/image/cultural/cultural11.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "The place where the king made speeches",
                        image: "/image/cultural/cultural12.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural13.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural14.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural15.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural16.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural17.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural18.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural19.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural20.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural22.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "Andranotapahina lake",
                        image: "/image/cultural/cultural23.jpg",
                        loc: "Antananarivo"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural24.jpg",
                        loc: "Antananarivo",
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural25.jpg",
                        loc: "Antananarivo"
                    }
                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {
                        image: "/image/cultural/culturalA1.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/cultural/culturalA2.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/cultural/culturalA3.jpg",
                        loc: "Ambohimanjaka"
                    }
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "Ancient Sakalava tomb",
                        image: "/image/cultural/culturalFF1.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "Ancient Sakalava tomb",
                        image: "/image/cultural/culturalFF2.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "Ancient Sakalava tomb",
                        image: "/image/cultural/culturalFF3.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF4.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF6.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF7.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF8.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF12.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF14.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF15.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF16.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF17.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF18.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF19.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF20.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF21.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF22.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF23.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF24.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF25.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF26.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalFF27.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF29.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF31.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF32.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF34.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF35.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF38.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF40.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF41.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF42.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF43.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF44.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF45.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF46.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF47.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF48.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF49.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF50.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF51.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF52.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF53.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF54.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF55.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF56.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF57.jpg",
                        loc: "Fianarantsoa"
                    },
                    {
                        name: "",
                        image: "/image/cultural/culturalF58.jpg",
                        loc: "Fianarantsoa"
                    },
                ]
            }
        ]
    },

    chameleon: {
        favori: "/image/chameleon/favori.jpg",
        description: "Chameleons is one of the highlight reptiles species in the island because half of the world chameleon are found on Madagascar, which grouped in three endemic genera, Calumma genus are largely found on the tropical rainforest which include thirty species.  Furcifer tend to be seen in broader distribution with eighteen species, Brookesia also have larger distribution with twenty four recognized.",
        imagesByLocalisation: [
            {
                localisation: "Tulear",
                species: [
                    {
                        name: "Warty chameleon",
                        image: "/image/chameleon/chameleon9.jpg",
                        video: "",
                        loc: "Tulear"
                    },
                    {
                        name: "Oustalets's chameleon",
                        image: "/image/chameleon/chameleon8.jpg",
                        video: "",
                        loc: "Tulear"
                    },
                    {
                        name: "Oustalets's chameleon female",
                        image: "/image/chameleon/chameleon2.jpg",
                        video: "",
                        loc: "Tulear"
                    }
                ]
            },
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Parson's chameleon",
                        image: "/image/chameleon/chameleon3.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Baby parson's chameleon",
                        image: "/image/chameleon/chameleon6.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Short horned chameleon",
                        image: "/image/chameleon/chameleon7.jpg",
                        video: "https://www.youtube.com/embed/tzzwFkxxgd8",
                        loc: "Andasibe"
                    },
                    {
                        name: "Nose horned chameleon",
                        image: "/image/chameleon/chameleon1.jpg",
                        video: "https://www.youtube.com/embed/tzzwFkxxgd8",
                        loc: "Andasibe"
                    },
                    {
                        name: "Perinet chameleon",
                        image: "/image/chameleon/chameleon4.jpg",
                        video: "",
                        loc: "Andasibe"
                    }

                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {
                        image: "/image/reptile/reptileA1.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/reptile/reptileA2.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/reptile/reptileA3.jpg",
                        loc: "Ambohimanjaka"
                    }
                ]
            }
        ]
    },

    geological: {
        favori: "/image/geological/favori.jpg",
        description: "The supercontinent of Gaondwanna masse land had been marked by collisional events and resulted in the birth of Madagascar. As a consequence of this deep geological relationship, the Island’s foundations are comprised of ancient continental crust, some of which can be considered among the world’s oldest rock. Inselberg are isolated mountains mainly granitic or gneissic rock outcrops rising abruptly above surrounding plains and typical landscapes features on Madagascar’s high plateau. Inselberg form ancient landscape features dating dozens years. They are characterized with vegetation type and plant community which are mostly endemic to and common landscape feature on Madagascar central plateau.",
        imagesByLocalisation: [
            {
                localisation: "Diego Suarez",
                species: [
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy1.jpg",
                        video: "https://www.youtube.com/embed/zLRv5_LoF24",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy2.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy3.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy4.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy7.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy9.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy10.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy13.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Red tsingy",
                        image: "/image/tsingy/tsingy14.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    }
                ]
            },
            {
                localisation: "Morondava",
                species: [
                    {
                        name: "",
                        image: "/image/geological/geologicalM55.jpg",
                        loc: "Bemaraha"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalM56.jpg",
                        loc: "Bemaraha"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalM57.jpg",
                        loc: "Bemaraha"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalM58.jpg",
                        loc: "Bemaraha"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalM59.jpg",
                        loc: "Bemaraha"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalM60.jpg",
                        loc: "Bemaraha"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalM61.jpg",
                        loc: "Bemaraha"
                    }
                ]
            },
            {
                localisation: "Mahajanga",
                species: [
                    {
                        name: "Zebu status",
                        image: "/image/cultural/cultural27.jpg",
                        video: "",
                        loc: "Ambondromamy"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural28.jpg",
                        video: "https://www.youtube.com/embed/aixEhpgzZ5Y",
                        loc: "Ambondromamy"
                    },
                    {
                        name: "",
                        image: "/image/cultural/cultural29.jpg",
                        video: "",
                        loc: "Ambondromamy"
                    }
                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {

                        name: "Granitic formation",
                        image: "/image/geological/geological48.jpg",
                        video: "https://www.youtube.com/embed/m2h1FvaUTlE",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/geological/geological5.jpg",
                        video: "https://www.youtube.com/embed/wvoPFNtS2L8",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Granitic tsingy like",
                        image: "/image/geological/geologicalA31.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological7.jpg",
                        loc: "Ambohimanjaka",
                        video: "https://www.youtube.com/embed/UEcnDBEdeVg",
                    },
                    {

                        image: "/image/geological/geological35.jpg",
                        video: "https://www.youtube.com/embed/P2AgLMR3ouY",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA1.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA2.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA3.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA4.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA5.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA6.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA7.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA9.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA10.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA11.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA12.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA13.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA14.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA15.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA16.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA17.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA18.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA19.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA20.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA21.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA22.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA23.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA24.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA25.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA30.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA32.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA33.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA34.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA35.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA36.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA37.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA38.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA39.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA40.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA41.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA42.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA43.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA45.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA50.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA51.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA52.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA53.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA54.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologicalA55.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Granitic tsingy like",
                        image: "/image/other/statut_maki.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Granitic tsingy like",
                        image: "/image/geological/geological1.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },

                    {
                        image: "/image/geological/geological6.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological8.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological10.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological32.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological33.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological34.jpg",
                        video: "",
                        loc: "AntsiraAmbohimanjakabe"
                    },
                    {

                        image: "/image/geological/geological36.jpg",
                        video: "",
                        loc: "AntsirAmbohimanjakaabe"
                    },
                    {

                        image: "/image/geological/geological37.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological38.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological39.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological40.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological41.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological42.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological43.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological44.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological45.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological46.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological47.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological49.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological50.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological51.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological52.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {

                        image: "/image/geological/geological53.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    }

                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "Isalo national parc",
                        image: "/image/geological/geological2.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Sandstone massif",
                        image: "/image/geological/geological3.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Window of Isalo",
                        image: "/image/geological/geological4.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Isalo national parc",
                        image: "/image/geological/geologica54.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologica55.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "",
                        image: "/image/geological/geologica56.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Geological Inselberg",
                        image: "/image/geological/geological11.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological12.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological13.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological14.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological15.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological16.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological17.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological18.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological19.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    // {
                    //     image: "/image/geological/geological20.jpg",
                    //     video: "",
                    //     loc: "Anja Park (Ambalavao)"
                    // },
                    {
                        image: "/image/geological/geological22.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological23.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological24.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological25.jpg",
                        video: "https://www.youtube.com/embed/OEqhLQEzCw8",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological26.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geological27.jpg",
                        video: "",
                        loc: "Anja Park (Ambalavao)"
                    },
                    {
                        image: "/image/geological/geologicalF1.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF2.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF3.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF12.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF14.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF15.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF17.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF21.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF22.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF23.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF24.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/geological/geologicalF25.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    }
                ]
            }
        ]
    },

    amphibian: {
        favori: "/image/amphibian/favori.jpg",
        description: "Malagasy frogs constitute one of the world’s richest groups of amphibians, with about 402 described species and some further candidates species are waiting to be named. All native and roughly 80% of genera and are endemic to Madagascar and neighboring islands. Tree frogs genus Boophis is of the most species rich endemic amphibians group of Madagascar.",
        imagesByLocalisation: [
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Madagascar bright eyed frog",
                        image: "/image/amphibian/amphibian1.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Pandanus frog",
                        image: "/image/amphibian/amphibian3.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Boophis madagascariensis",
                        image: "/image/amphibian/amphibian2.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Boophis luteus",
                        image: "/image/amphibian/amphibian5.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Blue-back reed frog",
                        image: "/image/amphibian/amphibian10.jpg",
                        video: "https://www.youtube.com/embed/HE9uIvAiPhs",
                        loc: "Andasibe"
                    },
                    {
                        name: "Blue-back reed frog",
                        image: "/image/amphibian/amphibian11.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Guibemantis species",
                        image: "/image/amphibian/amphibian12.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Central bright eyed frog",
                        image: "/image/amphibian/amphibian13.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Mantidactylus species",
                        image: "/image/amphibian/amphibian14.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Boophis viridis",
                        image: "/image/amphibian/amphibian15.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Spinomantis aglavei",
                        image: "/image/amphibian/amphibian16.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Lichen treefrog",
                        image: "/image/amphibian/amphibian17.jpg",
                        video: "",
                        loc: "Andasibe"
                    }
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "",
                        image: "/image/amphibian/amphibian20.jpg",
                        loc: "Ranomafana"
                    },
                    {
                        name: "",
                        image: "/image/amphibian/amphibian21.jpg",
                        loc: "Ranomafana"
                    },
                    {
                        name: "",
                        image: "/image/amphibian/amphibian23.jpg",
                        loc: "Ranomafana"
                    }
                ]
            }
        ]
    },

    handicraft: {
        favori: "/image/handicraft/favori.jpg",
        description: "Madagascar has a wide array of handicraft products ranging from basketry, mats, ceramics, pottery, hand textiles and woven products, woodcraft, and is the only African country having an old traditional silk sector.Handicrafts production in Madagascar  is cultural, traditional and predominantly a supplement household incomes. It is possible for visitors to visit  any workshop of those artisans activity.",
        imagesByLocalisation: [
            {
                localisation: "Ambositra",
                species: [
                    {
                        image: "/image/handicraft/handicraft1.jpg",
                        video: "",
                        loc: "Ambositra"
                    },
                    {
                        image: "/image/handicraft/handicraft2.jpg",
                        video: "",
                        loc: "Ambositra"
                    },
                    {
                        image: "/image/handicraft/handicraft3.jpg",
                        video: "",
                        loc: "Ambositra"
                    }
                ]
            },
            {
                localisation: "Tamatave",
                species: [
                    {
                        image: "/image/handicraft/handicraft6.jpg",
                        video: "https://www.youtube.com/embed/JTon8qb7kwc",
                        loc: "Moramanga"
                    },
                    {
                        image: "/image/handicraft/handicraft7.jpg",
                        video: "https://www.youtube.com/embed/YmbVEZTWlZs",
                        loc: "Moramanga"
                    },
                    {
                        image: "/image/handicraft/handicraft8.jpg",
                        video: "",
                        loc: "Moramanga"
                    },
                    {
                        image: "/image/handicraft/handicraft9.jpg",
                        video: "https://www.youtube.com/embed/HEPhsg_5Ao",
                        loc: "Moramanga"
                    },
                    {
                        image: "/image/handicraft/handicraft10.jpg",
                        video: "khttps://www.youtube.com/embed/XULuYbXxHFg",
                        loc: "Moramanga"
                    },
                    {
                        image: "/image/handicraft/handicraft11.jpg",
                        video: "https://www.youtube.com/embed/3QrStqs8TZM",
                        loc: "Moramanga"
                    }
                ]
            },
            {
                localisation: "Manakara",
                species: [
                    {
                        image: "/image/handicraft/handicraft12.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft13.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft14.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft15.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft17.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft18.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft19.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft20.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft21.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft22.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft23.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft24.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft25.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft26.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft27.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft28.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft29.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/handicraft/handicraft30.jpg",
                        video: "",
                        loc: "Manakara"
                    }
                ]
            },
        ]
    },

    reptile: {
        favori: "/image/reptile/favori.jpg",
        description: "Lizards, Ghecko and Iguana are species rich group of reptile mainly inhabiting in Madagascar, they are distributed and found in great variety of habitat.Gheckonideae family is  one of the most diversified families, majorities of the genera are nocturnal but Phelsuma genus is the most common seen day active species.",
        imagesByLocalisation: [
            {
                localisation: "Diego Suarez",
                species: [
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile1.jpg",
                        video: "https://www.youtube.com/embed/oNFGSWcQnhs",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile2.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile3.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    }
                ]
            },
            {
                localisation: "Tulear",
                species: [
                    {
                        name: "Madagascar Iguana",
                        image: "/image/reptile/reptile9.jpg",
                        video: "https://www.youtube.com/embed/-Brwi3LZthA",
                        loc: "Ifaty"
                    },
                    {
                        name: "Thick Tail Gecko",
                        image: "/image/reptile/reptile4.jpg",
                        video: "https://www.youtube.com/embed/erOvFRV061c",
                        loc: "Ifaty"
                    },
                    {
                        name: "Genus Trachylepis",
                        image: "/image/reptile/reptile5.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Merrem's Madagascar Swift",
                        image: "/image/reptile/reptile6.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Day Geckos",
                        image: "/image/reptile/reptile7.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Chalarodon madagascariensis",
                        image: "/image/reptile/reptile8.jpg",
                        loc: "Ifaty",
                        video: "",
                    }
                ]
            },
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Mossy Leaf-tailed Gecko",
                        image: "/image/reptile/reptile10.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Leaf-tailed Gecko",
                        image: "/image/reptile/reptile11.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Striped Day Gecko",
                        image: "/image/reptile/reptile13.jpg",
                        video: "",
                        loc: "Andasibe"
                    }
                ]
            },
            {
                localisation: "Mahajanga",
                species: [

                    {
                        name: "Cuvier's Madagascar Swift",
                        image: "/image/reptile/reptile15.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Cuvier's Madagascar Swift",
                        image: "/image/reptile/reptile16.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Girdled Lizard",
                        image: "/image/reptile/reptile19.jpg",
                        video: "https://www.youtube.com/embed/127C0zRbWIc",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile20.jpg",
                        video: "https://www.youtube.com/embed/qv__tu2ps6o",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile22.jpg",
                        video: "https://www.youtube.com/embed/97RUXpp8sPU",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile23.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Cuvier's Madagascar Swift",
                        image: "/image/reptile/reptile24.jpg",
                        video: "https://www.youtube.com/embed/LUEQ1drkwgU",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile25.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Nile crocodile",
                        image: "/image/reptile/reptile26.jpg",
                        video: "https://www.youtube.com/embed/7BAMXxUQO_Q",
                        loc: "Ankarafantsika"
                    },
                    {
                        name: "Cuvier's Madagascar Swift",
                        image: "/image/reptile/reptile28.jpg",
                        video: "",
                        loc: "Ankarafantsika"
                    }
                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {
                        name: "Oplirus quadrimaculatus",
                        image: "/image/reptile/reptile29.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Oplirus quadrimaculatus",
                        image: "/image/reptile/reptile30.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Gold spotted mabuya",
                        image: "/image/reptile/reptile31.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    }
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "",
                        image: "/image/reptile/reptileF14.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF15.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF1.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF2.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF3.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF4.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF6.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF7.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF8.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF12.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/reptile/reptileF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },

                ]
            }
        ]
    },

    flora: {
        favori: "/image/flora/favori.jpg",
        description: "Madagascar is the world’s largest island and hosts an extraordinary number of endemic floras. Over 80% of the flowering plants occur nowhere else in the world. The outstanding proportion of endemic species is concentrated in ten families: ORCHIDACAE are the family with the largest number species, over 900 species of orchids discovered in different habitat existing in the island.",
        imagesByLocalisation: [
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Angraecum genus",
                        image: "/image/flora/flora31.jpg",
                        video: "https://www.youtube.com/embed/j1eYQfuT17w",
                        loc: "Andasibe"
                    },
                    {
                        name: "Bulbophyllum genus",
                        image: "/image/flora/flora32.jpg",
                        video: "https://www.youtube.com/embed/dpyIseWlgcE",
                        loc: "Andasibe"
                    },
                    {
                        name: "Angraecum genus",
                        image: "/image/flora/flora33.jpg",
                        video: "https://www.youtube.com/embed/UhY1lmUfkAs",
                        loc: "Andasibe"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraT1.jpg",
                        loc: "Tamatave"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraT2.jpg",
                        loc: "Tamatave"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraT3.jpg",
                        loc: "Tamatave"
                    },
                    {
                        name: "Bulbophyllum analamazoatrae",
                        image: "/image/flora/flora2.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Strongylodon madagascariensis",
                        image: "/image/flora/flora3.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Nepenthes madagascariensis",
                        image: "/image/flora/flora7.jpg",
                        video: "",
                        loc: "Akanin'ny Nofy"
                    },
                    {
                        name: "Aerangis hyaloides",
                        image: "/image/flora/flora6.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Oeania rosea",
                        image: "/image/flora/flora14.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Chassalia acutiflora",
                        image: "/image/flora/flora12.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Hypoestes declipteroides",
                        image: "/image/flora/flora13.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Darwin's orchid",
                        image: "/image/flora/flora20.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Angraecum eburneum",
                        image: "/image/flora/flora15.jpg",
                        video: "",
                        loc: "Akanin'ny Nofy"
                    }

                ]
            },
            {
                localisation: "Morondava",
                species: [
                    {
                        name: "",
                        image: "/image/baobab/baobab10.jpg",
                        loc: "Morondava"
                    },
                    {
                        name: "",
                        image: "/image/baobab/baobab11.jpg",
                        loc: "Morondava"
                    },
                    {
                        name: "",
                        image: "/image/baobab/baobab12.jpg",
                        loc: "Morondava"
                    }
                ]
            },
            {
                localisation: "Diego Suarez",
                species: [
                    {
                        name: "",
                        image: "/image/baobab/baobab7.jpg",
                        video: "https://www.youtube.com/embed/fhvcOvxxCVw",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "",
                        image: "/image/baobab/baobab8.jpg",
                        video: "",
                        loc: "Diego Suarez"
                    },
                    {
                        name: "",
                        image: "/image/baobab/baobab9.jpg",
                        video: "https://www.youtube.com/embed/ghorrq3_xxE",
                        loc: "Diego Suarez"
                    }
                ],
            },
            {
                localisation: "Tulear",
                species: [
                    {
                        name: "Adansonia rubrostipa",
                        image: "/image/baobab/baobab1.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Adansonia rubrostipa",
                        image: "/image/baobab/baobab2.jpg",
                        video: "https://www.youtube.com/embed/a1upvJWR8Xo",
                        loc: "Ifaty"
                    },
                    {
                        name: "Adansonia rubrostipa",
                        image: "/image/baobab/baobab3.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Adansonia rubrostipa",
                        image: "/image/baobab/baobab5.jpg",
                        video: "",
                        loc: "Ifaty"
                    },
                    {
                        name: "Stereospermum",
                        image: "/image/flora/flora1.jpg",
                        video: "",
                        loc: "Berenty"
                    },
                    {
                        name: "Didierea trollii",
                        image: "/image/flora/flora5.jpg",
                        video: "",
                        loc: "Berenty"
                    },
                    {
                        name: "Delonix pumila",
                        image: "/image/flora/flora21.jpg",
                        video: "",
                        loc: "Berenty"
                    }
                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {
                        name: "Pachypodium brevicaule",
                        image: "/image/flora/floraA1.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Huperzia",
                        image: "/image/flora/floraA2.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Daizy family",
                        image: "/image/flora/floraA4.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA5.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA6.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Billy goat weed",
                        image: "/image/flora/floraA7.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Ipomoea batata ",
                        image: "/image/flora/floraA8.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA9.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA10.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA11.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA12.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA13.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA15.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Borassus madagascariensis",
                        image: "/image/flora/floraA16.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA17.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA18.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Aloe genus",
                        image: "/image/flora/floraA19.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA20.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Stem suculent plant",
                        image: "/image/flora/floraA21.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "Xerophyta",
                        image: "/image/flora/floraA22.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA23.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA24.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA25.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA26.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA27.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA26.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA27.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA28.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA29.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA30.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA31.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA32.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraA33.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        name: "Albigizia gummifera",
                        image: "/image/flora/flora19.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Bismarck Palm",
                        image: "/image/flora/flora4.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "Pachypodium rosulatum",
                        image: "/image/flora/flora22.jpg",
                        video: "",
                        loc: "Isalo"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF1.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF2.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "Bulbophyllum",
                        image: "/image/flora/floraF3.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "Elephant's foot plant",
                        image: "/image/flora/floraF4.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF6.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF7.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF8.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF12.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF14.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF15.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF16.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF17.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF18.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF19.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF20.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF21.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF22.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF24.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF25.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF26.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        name: "",
                        image: "/image/flora/floraF27.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    }
                ]
            },
        ]
    },

    landscape: {
        favori: "/image/landscape/favori.jpg",
        description: "The island of Madagascar contains a diversity of landscapes such as: hills terraced by rice paddies in various shades of green, yellow and brown, vast plains of golden pasture, lush tropical rain forest, massive granitic inselbergs  and mountain ranges, palm - lined  tropical beaches and cobblestone lined urban streets and many others. Madagascar is an amazing destination because of each diverse scenery and interest by landscape photographer  and scenery admirer. On your traveling on Madagascar, you will be attracted by those various  typical landscape, as its region has their own particularity.",
        imagesByLocalisation: [
            {
                localisation: "Nosy Be",
                species: [
                    {
                        carousel: {
                            title: "NOSY ANTSOHA",
                            description: "One of the islet around Nosy Be"
                        },
                        name: "",
                        image: "/image/beach/beach1.jpg",
                        loc: "Nosy Be"
                    },
                    {
                        carousel: {
                            title: "Surronding Hellville Port",
                            description: "And the region coastal marine ecosystems"
                        },
                        name: "",
                        image: "/image/beach/beach2.jpg",
                        loc: "Nosy Be"
                    },
                    {
                        carousel: {
                            title: "MONT PASSOT",
                            description: "A spectacular panoramic view from highest peak of Nosy Be"
                        },
                        name: "",
                        image: "/image/beach/beach3.jpg",
                        loc: "Nosy Be"
                    },
                    {
                        carousel: {
                            title: "NOSY BE",
                            description: "And its resort luxurious Hotel."
                        },
                        name: "",
                        image: "/image/beach/beach4.jpg",
                        loc: "Nosy Be"
                    },
                    {
                        carousel: {
                            title: "NOSY BE",
                            description: "Broad marginal beach of quartz sand with deliciously scented vegetation"
                        },
                        image: "/image/beach/beach5.jpg",
                        loc: "Nosy Be"
                    }
                ]
            },
            {
                localisation: "Fianarantsoa",
                species: [
                    {
                        image: "/image/landscape/landscape1.jpg",
                        video: "https://www.youtube.com/embed/_TYDW9EIyYw",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape2.jpg",
                        video: "https://www.youtube.com/embed/KKhrgMcCSaU",
                        loc: "Ranomafana"
                    },
                    {
                        name: "Madagascar's typical lavaka",
                        image: "/image/landscape/landscape4.jpg",
                        video: "https://www.youtube.com/embed/sXIBmKIJFQY",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape5.jpg",
                        video: "https://www.youtube.com/embed/i2fKiPBnJdI",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape6.jpg",
                        video: "https://www.youtube.com/embed/VrHPYIGwws8",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape7.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape9.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape10.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape11.jpg",
                        video: "https://www.youtube.com/embed/UzQidUi_Fl4",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape12.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscape13.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        image: "/image/landscape/landscapeF1.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF2.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF3.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF4.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF5.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF6.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF7.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF8.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF9.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF10.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF11.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF12.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF3.jpg",
                        loc: "Tsaranoro (Andrigitra)",
                    },
                    {
                        image: "/image/landscape/landscapeF14.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF15.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF13.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF16.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF17.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF19.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF4.jpg",
                        loc: "Tsaranoro (Andrigitra)",
                    },
                    {
                        image: "/image/landscape/landscapeF20.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF23.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF24.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF25.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF26.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF27.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF28.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF29.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/plaque/plaqueF8.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF30.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF31.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF32.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    },
                    {
                        image: "/image/landscape/landscapeF33.jpg",
                        loc: "Tsaranoro (Andrigitra)"
                    }
                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {
                        image: "/image/landscape/landscape14.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape32.jpg",
                        video: "https://www.youtube.com/embed/9YfmLPtHI_o",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape15.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape29.jpg",
                        video: "https://www.youtube.com/embed/4JKFX7mWpG4",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape16.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape36.jpg",
                        video: "https://www.youtube.com/embed/7KUPJiNLvDc",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape17.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape51.jpg",
                        video: "https://www.youtube.com/embed/Jpc7zdBxWTU",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape19.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape37.jpg",
                        video: "https://www.youtube.com/embed/BYPYO-Ogz_s",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape30.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape71.jpg",
                        video: "https://www.youtube.com/embed/KWTl33ybiCc",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape18.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape31.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA1.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA2.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA3.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA4.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA5.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA6.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA7.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA8.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA9.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA10.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA11.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA12.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA13.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA14.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA15.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA16.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA17.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA18.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA19.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA20.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA21.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA22.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA23.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA24.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeAA25.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape33.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape34.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape35.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },

                    {
                        image: "/image/landscape/landscape39.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape40.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape41.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape42.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape43.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape45.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape46.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape47.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape48.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape49.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape50.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },

                    {
                        image: "/image/landscape/landscape52.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape53.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape54.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape55.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape56.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape57.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape58.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape59.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape60.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape61.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape62.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape63.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape64.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape65.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape66.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape67.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape68.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape69.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape70.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape72.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape73.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscape75.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA1.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA2.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA3.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA4.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA5.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA6.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA7.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA8.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA9.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA10.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA11.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA12.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA13.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA14.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA15.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA16.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA17.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA18.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA19.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA20.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA21.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA22.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA23.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA24.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA25.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA27.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA28.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA29.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA30.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA31.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA32.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA33.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA34.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/landscape/landscapeA35.jpg",
                        video: "",
                        loc: "Ambohimanjaka"
                    }

                ]
            },
            {
                localisation: "Mahajanga",
                species: [
                    {
                        name: "Lake Ravelobe",
                        image: "/image/landscape/landscape76.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape86.jpg",
                        video: "https://www.youtube.com/embed/wV7dCLdk_QQ",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape77.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape78.jpg",
                        video: "https://www.youtube.com/embed/vmmc8wbXJ6U",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape79.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape81.jpg",
                        video: "https://www.youtube.com/embed/VPabLWZmXNY",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape80.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape82.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape83.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape84.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape85.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },

                    {
                        image: "/image/landscape/landscape87.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape88.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape89.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape90.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape91.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape92.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape93.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    },
                    {
                        image: "/image/landscape/landscape105.jpg",
                        video: "",
                        loc: "Ankarafatsika"
                    }
                ]
            },
            {
                localisation: "Manakara",
                species: [
                    {
                        name: "Pangalanne canal",
                        image: "/image/landscape/landscape96.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscape97.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscape98.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscape99.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscape100.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        name: "Train station",
                        image: "/image/landscape/landscape101.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscape102.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        name: "Manakara city",
                        image: "/image/landscape/landscape103.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscape104.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        name: "An old colonial infrastructure",
                        image: "/image/landscape/landscapeM1.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        name: "Avicennia marina",
                        image: "/image/landscape/landscapeM2.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscapeM3.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscapeM4.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscapeM5.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscapeM6.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscapeM7.jpg",
                        video: "",
                        loc: "Manakara"
                    },
                    {
                        image: "/image/landscape/landscapeM8.jpg",
                        video: "",
                        loc: "Manakara"
                    }
                ]
            },

        ]
    },

    insect: {
        favori: "/image/insect/favori.jpg",
        description: "",
        imagesByLocalisation: [
            {
                localisation: "Tamatave",
                species: [
                    {
                        name: "Darwin's bark spider",
                        image: "/image/insect/insect1.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Giraffe weevil",
                        image: "/image/insect/insect11.jpg",
                        video: "https://www.youtube.com/embed/fNsvlM9POcA",
                        loc: "Andasibe"
                    },
                    {
                        name: "Fungus weevil",
                        image: "/image/insect/insect2.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Katydid",
                        image: "/image/insect/insect3.jpg",
                        video: "https://www.youtube.com/embed/E2TPEfQDto8",
                        loc: "Andasibe"
                    },
                    {
                        name: "Long horned beetle",
                        image: "/image/insect/insect4.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Madagascar moon moth",
                        image: "/image/insect/insect5.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Metallic wood boring beetle",
                        image: "/image/insect/insect6.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Net casting spider",
                        image: "/image/insect/insect7.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Round backed millipede",
                        image: "/image/insect/insect8.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Verdant hawckmoth",
                        image: "/image/insect/insect9.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Click beetle (Berenty)",
                        image: "/image/insect/insect10.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Colossopus redtenbacheri",
                        image: "/image/insect/insect12.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Pandanus weevil",
                        image: "/image/insect/insect13.jpg",
                        video: "",
                        loc: "Andasibe"
                    },
                    {
                        name: "Bark spider",
                        image: "/image/insect/insect14.jpg",
                        video: "",
                        loc: "Ranomafana"
                    },
                    {
                        name: "Long horned beetle",
                        image: "/image/insect/insect15.jpg",
                        video: "",
                        loc: "Andasibe"
                    }
                ]
            },
            {
                localisation: "Antsirabe",
                species: [
                    {
                        image: "/image/insect/insect16.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/insect/insect17.jpg",
                        loc: "Ambohimanjaka"
                    },
                    {
                        image: "/image/insect/insect18.jpg",
                        loc: "Ambohimanjaka"
                    }
                ]
            }
        ]
    }
}

export default data;