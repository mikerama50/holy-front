const data = {
    Tamatave: {
        description: "Tamatave Eastern region of Madagascar is coastal city which harbors the tropical rain-forest of the island, that  has an amazing diverse tropical flora diversity. This region has a unique concentration of different types of habitat and species of Madagascar. They fall into a few main categories of ecosystems: higher mountain forest, mid-altitude forest, lowland forest, and littoral forest, where majority of those endemic families and genera are found. Despite the species richness of the eastern region which is high, the forest  have been under ever increasing pressure    from human demands for fuel, cropland and pasture some species should be considered  endangered status by the IUNCN to experience the Madagascar best biodiversity, eastern region is proposed site as it contains a significant number of animal species.",
        circuits: ['east', 'wildlife_watching', 'birding_tour']
    },

    Tulear: {
        description: "Toliary is a capital city of the southwest of region, the city lies within the arid spiny thicket Eco region. The region has the highest of plant endemism in Madagascar and is listed as one of the most important ecological region in the world. Southwest Madagascar exhibit high endemism for plants,  bird , mammals and reptiles. Majority of the plants and animals of the spiny forest are locally endemic in this region. Southwest part of Madagascar has a highly seasonal environment that has favored the evolution of diverse strategies to cope with environmental stress. Visiting Madagascar, tropical dry area is among the site, which is really interesting to learn those incredible adaptive response to various environmental stress.",
        circuits: ['south', 'birding_tour']
    },

    Fianarantsoa: {
        description: "Fianarantsoa is component part of the central highland of Madagascar and is an important ecoregional preserving the highland forest corridor between Ranomafana and Andringitra National Park. The corridor in the Fianarantsoa region is actually part of a longer corridor that stretches over much of the Malagasy highland is a 450 km long band of forest. The parks in Fianarantsoa region and the corridor between them represent interesting ecosystems because of the great altitude differences in relatively divers area. Maintaining and preserving  the ecosystem in this region is crucial both for human socio-economic and different animals community, however the habitat.",
        circuits: ['south', 'trekking_tour', 'birding_tour']
    },

    Ambositra: {
        circuits: ['zafimaniry'],
        description:"Ambositra is situated in the central of Madagscar and is characterized by high relief and impressive terraces of rice paddies, also part of the central highland of Madagascar, home to the Betsileo ethnic group which is also known as Betsileo north. The region has become renowned center for woodcarving and marquetry. The region is among the geographical distribution of tapia woodland forest, an endemic forest exist in central highland of Madagascar, which harbor many benefits, including wild silkworms,edible insecte and herbal medicines. The region's inselberg granitic and gneiss outcrop offer a potential for tourist rock climbng and with remarkably unique flora and vegetation which are outstanding rich in terms of both species diversity and  endemicity."
    },

    Manakara: {
        description: "Manakara is an ancient colonial city located on the Southeastern coast of Madagascar, capital of the Vatovavy fitovinany region. This city is home of the Antehimoro people and well known in their artisanal manufacture of Antehimoro paper. It is an area shaded by Filao trees, surrounding by luxuriant green vegetation and unique coastal diversity, which this forest harbor a very unusual community. The city is part of the area where the largest canal is found an was very important for the economic reason since the colonization period up to know. Several cash agricultural products are produced in the region such as: coffee, litchi, vanilla, …",
        circuits: ['southeast']
    },

    Mahajanga: {
        description: "Western Madagascar region comprises karstic landscapes of an unique limestone massif pinnacles and different soil types includes unconsolidated sand where the typical dry deciduous forest in habit. The western Malagasy forest support numerous endemic species and are known for its biodiversity richness, although the number of sampled sites is a relatively small compared to humid forest. The vegetation consist of dry deciduous forest like Commiphora, Dalbergia and Hildegardea are the dominant canopy tree reaching up to 20m which supporting rich faunal locally  endemic community, such as: vandam’s vanga, coquerels coua among the endemic bird species, and range of several endangered mammal like coquerel Sifaka. Mahajanga western region contains one of the five largest fragments still remaining of western primary forest.",
        circuits: ['northwest']
    },

    Morondava: {
        description: "Morondava is the capital of the Menabe region, and is located in the southwestern part of Madagascar, which holds the largest still existing block of western dry forest, the Menabe forest including, Kirindy and Andranomena forest. The central Menabe region harbors the largest remnant of dry forest in Western Madagascar. These forest are home to high floral and faunal diversity including a number of local and regional endemics. The forest in the region are classified “western dry forest” are seasonally dry deciduous forest ranging from: the giant jumping rat, the largest living endemic rodent of Madagascar, the only population of Madame Berthe’s mouse lemur, the smallest primate of the world, including the striped mongoose and White- breasted mesite. Central menabe forest provide the sole habited for several endangered endemic species including: the animal and plants inhabiting this ecosystem have developed remarkable adaptation to cope with the marked seasonality of their habitat.",
        circuits: ['southwest']
    },

    "Diego Suarez": {
        description: "Diego Suarez is a large city in the northern part  of Madagascar, the capital of Antakarana ethnic groups ,it used to be part of the French foothold  during the colonisation period and an ancient place of  military base. The vegetation in the region cosists of  tropical humid and dry  forests,  one of the most spectacular areas for its vast set of islets, with a vast plateau of sandy corral a few kilometers long. Some part of  Diego Suarez region was foremed volcanic eruption whereas the other part geological dominated by karstik  formation.   The structure and natural resources of the region  are varied in each region, in  which the  endemic species of flora and fauna are thrive  in  the different natural environments.",
        circuits: ['north']
    },

    "Nosy Be" : {
        description: "Nosy be and the surrounding nearby islands are located in the North- west of Madagascar, with a total surface area of about 25, 200 ha and a maximum altitude of 430 m. It is the largest off shore island of Madagascar, and is included within the Sambirano Domain, which is characterized by a vegetation similar in many aspects to that of the Eastern rainforest. Nosy be falls within the Province of Diego, its distance from the nearest mainland sites is Ankify to the South. This treasure island is an important Centre for tourism and agriculture, several cash crops grown in the island include coffee, ylang-ylang, and sugar cane for the production of essential oils. Considering the unique flora and fauna within the Lokobe primary forest located in the southeastern part of Nosy be, this destination is the best both for marine activty and wildlife watching as well as relaxation.",
        circuits: ['north']
    },

    Antsirabe: {
        circuits: ['ambohimanjaka'],
        description:"Antsirabe is the second largest town in Madagascar in terms of population. It is the agricultural and industrial center of the island, due to the vast and dark fertile land, many agricultural products are produced in the region. The town is also known for thermal springs, because of the large volcanic terrain of the area, and is widely visit by local people for the water therapeutic purpose.The region is also important archaelogical site of the island, where subfossil megafauna, including pygmy huppos, giant tortoise, and large lemurs were found. It is an area  remarkably by Ambohimanjaka massif located 40 km south of Antsirabe city,and reaches 2000 m in elevation, one of the highest inselberg granitic in the central higlhand. Ambohimanjaka mountain is dominated by vast tapia  woodlands, which few endemic plants  exist in this areas: aloe genus,xerophyta, suculent plants (pachypodium brevicaule, ...)"
    }
}

export default data;