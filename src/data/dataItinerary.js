const data = {
    masoala: {
        images: [
            '/image/lemurs/lemur11.jpg',
            '/image/bird/bird41.jpg',
            '/image/other/masoala.jpg'
        ],
        title: "Masoala National Parks",
        subtitle: "",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On arrival into Antananarivo Airport, where you will be welcomed by our local representative who helps you with different formalities, and transfer to Hotel.\n Overnight in Antananarivo",
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Antananarivo-Andasibe",
                        content: "This morning, you will depart on the 04H30 drives to Andasibe which is still part of the central domain. The journey enables you to know the people of the central island and knowing their daily economic activities. An introductory night walk. Stay three nights in Andasibe."
                    }
                ]
            },
            {
                name: "DAY 3-4",
                steps: [
                    {
                        title: "Analamazoatra and Mantadia National Parks",
                        content: "This is one of Madagascar’s most popular reserves. Those two parks are typical mid altitude moist mountain forest with altitude 920 to 1100 m where one can explore both secondary and primary forest. The vegetation is evergreen as the area is under the influence of the southeastern trade winds. And the canopy height can reach 25 m. The parks are home to a variety of lemurs, birds, reptiles and invertebrates and different kinds of nicely attracting insects. The main target is to look for the first and second largest living species of lemurs. Spend 2 days is exploring the forest with private local guide, searching for an amazing wildlife which reside here. This is one of the wonderful places in Madagascar that allows you to have a fantastic opportunity to experience some of Madagascar biodiversity species richness."
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "Anatananarivo",
                        content: "Breakfast and morning at leisure, this noon at a time to be confirmed the locally, you will be driven back to Antananarivo you transfer to the Lisy gallery for an overview of all of the handicrafts for which Madagascar is known.\nDinner and overnight at the hotel. "
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Antananarivo-Maroantsetra-Masoala National Park",
                        content: "After breakfast, you will be transferred to the Airport for your flight to northeast coast of the island. Upon arrival in Maroantsetra you will be taken one half hour drive from the Airport to the harbor and embark on speed boat to Masoala National Park.\nStay 3 nights."
                    }
                ]
            },
            {
                name: "DAY 7-8",
                steps: [
                    {
                        title: "Masoala National Park",
                        content: "Send two days exploring Madagascar’s largest remaining lowland rainforest block, the park contains diverse habitat including low and mid altitude littorale forest and mangrove, Masoala national park has a high canopy height near 30 m and high floristic diversity and steep mountainous topography , giving visitors a wide range of  rare and endemic species, icluding red ruffed lemurs that has a smaller geographic range occuring only in Masoala peninsula, some other impotrant species includes white fronted brown lemur and several important bird species: helmet vanga, blue coua, brown mesite, wood rail. Guided night walk."
                    }
                ]
            },
            {
                name: "DAY 9",
                steps: [
                    {
                        title: "Masoala – Maroantsetra-Antananarivo",
                        content: "Flight back to Antananarivo, after having explored the beautiful Masoala national park, afternoon leisure upon arrival in the capital. Overnight at the hote.",
                    }
                ]
            },
            {
                name: "DAY 10",
                steps: [
                    {
                        title: "international flight to Tana",
                        content: "Morning at a leisure, and transferred to airport for flights home. End of our services",
                    }
                ]
            }

        ]
    },

    trekking_tour: {
        images: [
            '/image/lemurs/lemur23.jpg',
            '/image/geological/geological28.jpg',
            '/image/lemurs/lemur1.jpg',
        ],
        title: "WILDLIFE WATCHING AND BEST TREKKING TOUR",
        subtitle: "RANOMAFANA Tropical humid forest - ANDRINGITRA  2nd highest mountain in Madagascar",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Arrival to Tana",
                        content: "On your arrival in Antananarivo, you will met by your guide and drive whom assist you changing some money, then driven to the hotel.\nDinner and overnight at the hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Tana-Ranomafana",
                        content: "Leaving early this morning after breakfast, you travel over 10 hours on the route National 7.Passing throughout he central Island of Madagascar. The journey is an amazing with different.\nWith different change in scenery from beautifully terraced ricefield to a land dominated by granite domes, as well as the change in people from hardworking rice cultivating Merina ethnic group to Betsileo even more industrious and skilled as rice cultivator Merina and craftsmen.\nThis is a quite a long day, but is on interesting drive with dramatic change in landscape, and well worth it once you get to a lush mountain rainforest area."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Ranomafana National",
                        content: "The park covers an area of 41 600 ha which contains a mixture of primary and mature secondary forest with mature canopy up to 30 m.\nThe park harbors a great diversity of primates and birds and also different kind faunal communities.\nTake a day visit into the Ranomafana National Park, home to the rare Milne – Edward Sifaka, and the golden and greater bamboo lemurs.\nWalk within the Talatakely trail located in low mountain rainforest to spot birds, lemurs before heading to the primary forest on up and down trail system, at a higher altitude of 1200 m.\n1h30 night walk along the edge of the forest to find herpetofaunas of the region.\nDinner and overnight at Hotel."
                    }
                ]
            },
            {
                name: "DAY 4",
                steps: [
                    {
                        title: "Ranomafana National Park-Ambalavao",
                        content: "Driving to Ambalavao, take a short guided walk into another site of Ranomafana National Park before getting back to the National Park 7.\nYou arrive in Ambalavao in the afternoon and visit the workshop to see the Art of spinning and weaving of silk, which is very ancient in Madagascar, and paper us type paper made of Avoha bark, was developed by Arabs to write Malagasy-Arab culture and custom by their script.\nOvernight at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "Ambalavao-Andringitra National Park(first camp site)",
                        content: "Andringitra National park is the second highest mountain and located in south central Madagascar. The park consist broad range of habitat: ranging from lowland forest-mountaine forest-sclerophyllous forest- mountain thicket to open area of bare rocks covered by geophytes.\nAfter breakfast, you will be transferred by vehicle about 3h30mn to the park entrance to meet the local guide and doing the formality.\nStart at the trekking on an easy and difficult trail about 4h, passing through the Betsileo villages, with short loop within Imaintso forest the first camp site."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Imaintso-Andriampotsy (second camp site)",
                        content: "Waking up with different bird songs and leaving the 1st camp site after breakfast, continuing the hike about 6h30mn within Iamaintso forest on a very difficult trail.\nA nice view overlooking the forest and village, and then climbing up over 2000 m high plateau to join your 2nd camp site Andriampotsy at the foot of huge granite cliffs."
                    }
                ]
            },
            {
                name: "DAY 7",
                steps: [
                    {
                        title: "Andriampotsy-Iataranomby (third camp site)",
                        content: "Wakening early with the first sunbeams and star continuing your adventure to discover the magnificent massif. Hiking and climbing up to 2658 m Madagascar 2nd highest peak, and descent toward the western side on your last camp site.\nA full-day journey."
                    }
                ]
            },
            {
                name: "DAY 8",
                steps: [
                    {
                        title: "Iataranomby-fianarantsoa",
                        content: "After a good night sleep and a breakfast, you will star going down to the valley for 4 hours, where you are finishing the trekking and with all these images in mind. You will take the car by the dirt road heading to Route National 7 for over an hour and half (1h30mn) before continuing towards Fianarantsoa.\nOvernight at Fianarantsoa."
                    }
                ]
            },
            {
                name: "DAY 9",
                steps: [
                    {
                        title: "Fianarantsoa-Tana",
                        content: "After breakfast, you will head back to Antananarivo."
                    }
                ]
            },
            {
                name: "DAY 10",
                steps: [
                    {
                        title: "Departure home",
                        content: "Today check out and spend the day relaxing before being transferred to the airport in time to check in for flight home."
                    }
                ]
            }

        ]
    },

    zafimaniry: {
        images: [
            '/image/zafimaniry/zafimaniry13.jpg',
            '/image/zafimaniry/zafimaniry17.jpg',
            '/image/zafimaniry/zafimaniry8.jpg',
        ],
        title: "ZAFIMANIRY",
        subtitle: "",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On arrival in Antananarivo you will be met by our local representative to assist you changing money and reconfirm your domestic flight, after that you will be driven to Overnight in the city hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Antananarivo-Ambositra",
                        content: "After breakfast: You will make your way south to admire the typical landscape, through the central highland to reach the volcanic area of the island. Along the way, you get to know the highlander people daily economic activities as well as their house and tomb based culture. The town is also known for thermal springs because of the large volcanic terrain of the area, and is widely visited by local people for the water therapeutic purpose. Afternoon city tour and visiting the family artistic economic livelihood. Overnight at the hotel"
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Ambositra-Antoetra –Sakaivo (1h30)",
                        content: "After breakfast, transfer by car to Antoetra, a 4 hours. journey that takes through typical Betsileo villages and the surrounding rice field. On the way; you will make your way to observe the largest highland and to explore the different woodcarving in the area. Bivouac"
                    }
                ]
            },
            {
                name: "DAY 4",
                steps: [
                    {
                        title: "Sakaivo-Ankidodo",
                        content: " Leaving early this morning after breakfast, trekking crossing valley, traditional house, and ancient luxuriant architecture resembling to  the old wooden house of Betsileo and Imerina. You will have the opportunity to venture into Ankidodo , you will find the cascade and the granitic  geological formation in the part. Overnight bivouac."
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "Ankidodo-Ifasiana-Antoetra-Ambositra",
                        content: "After breakfast, hiking and visit the village, and meet the  local community. Dinner camping and overnight."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Ambositra-Antananarivo",
                        content: "Return to Antanarivo"
                    }
                ]
            }

        ]
    },

    north: {
        images: [
            '/image/tsingy/tsingy1.jpg',
            '/image/reptile/reptile1.jpg',
            '/image/baobab/baobab9.jpg'
        ],
        title: "NORTH AND NORTHWEST OF MADAGASCAR",
        subtitle: "AMBER MOUNT- ANKARANA LIMESTONE –NOSY BE ISLAND",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On your arrival, you will be met by our local representative to assist you changing money and reconfirm your domestic flight, after that you will be driven to overnight at Hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "ANTANANARIVO – DIEGO",
                        content: "Today you will taken back to the airport for your flight to the northern part of Madagascar.  Upon arrival drive southward of Diego to Amber Mountain, passing through the outskirts of the city center of the region, and end up to Joffre the ancient old colonial town, which used to be part of the first French foothold in Madagascar. Overnight at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "AMBER MOUNTAIN NATIONAL PARK",
                        content: "Amber Mountain is volcanic origin composed of basaltic rock, on which the major floristic features of the Park consist of Mountain rainforest and forest herbaceous undergrowth and evergreen seasonal forest. The Park protects 18 200 ha of mid altitude rainforest lying altitude of ranging from 850-1475 m. The Park is one of Madagascar biodiversity and unique ecosystem, where you explore along with local guide to spot the smallest ground chameleon and key bird species and locally important mammal’s fauna, and highly camouflage leaf tailed geckos. Night walk along the edge of the forest. Overnight at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 4",
                steps: [
                    {
                        title: "AMBER MOUNTAIN- ANKARANA",
                        content: "After breakfast, you transfer approximately 5 hours to Ankarana area, on the way drive to the Red tsingy to find out the dominant Red lateritic soil type in tsingy like form. En route, you will learn and see the Antakarana ethnic group by their agricultural products based economic activity. You get settled and enjoy afternoon leisure. Overnight at the Lodge"
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "ANKARANA NATIONAL PARK",
                        content: "Ankarana National Park is best known for its massif, a limestone karst formation with 18 200 ha dry deciduous forest and numerous extensive cave systems with underground river and canyon, which is the richest bat sites on the island. Landscape viewing and wildlife watching is the main attraction of the Park, and is also home to some of Madagascar’s best known succulent plant species, such as Aloe, Pachypodium, Euphorbia, Baobab and many others. Dinner and overnight at the Lodge."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "ANKARANA – ANKIFY - NOSY BE ",
                        content: "After breakfast, we travel over two hours and half to the popular offshore northwestern Island of Nosy be, and take a thirty minutes speed boat to reach Nosy be. Enjoy afternoon leisure. Dinner at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 7-8",
                steps: [
                    {
                        title: "NOSY BE HIGHLITGHT",
                        content: "Nosy be is the largest off shore of Madagascar with a total surface area of 25 200 ha and maximum altitude of 430 m and is characterized by a vegetation similar in many aspect to that of the eastern rainforest. During your stay in Nosy be, you walk or relax on the beach or take optional boat trip for snorkeling, or an excursion into the surrounding islands in search for unique and rare wildlife. You may also visit ylang- ylang plantation market, including driving tour to the highest peak viewing."
                    }
                ]
            },
            {
                name: "DAY 9",
                steps: [
                    {
                        title: "NOSY BE–ANTANANARIVO",
                        content: "After breakfast and morning leisure, transfer to the Airport to Antananarivo. Upon arrival you are met with our local representative and to transfer to in your hotel for a day use."
                    }
                ]
            },
            {
                name: "DAY 10",
                steps: [
                    {
                        title: "DEPART on international flight home",
                        content: "..."
                    }
                ]

            }

        ]
    },

    south: {
        images: [
            '/image/gallery2/geological.jpg',
            '/image/gallery2/bird.jpg',
            '/image/baobab/baobab3.jpg',
            '/image/landscape/landscape1.jpg',
            '/image/other/antsirabe_pousse.jpg',
            '/image/other/trano.jpg'
        ],
        title: "SOUTHEASTERN – SOUTHERN – CENTRAL HIGHLAND",
        subtitle: "Tropical humid forest – Granitic inselberg – Sandstone massif – Transitional forest",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On your arrival in Antananarivo, you will met by your guide and drive whom assist you changing some money, then driven to the hotel. Dinner and overnight at the hotel"
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Tana–Antsirabe",
                        content: "You leave the hotel from Antananarivo in the morning and can admire the landscape through the central highland to reach the volcanic area of the island. Along the way, you get to know the highlander people daily economic activities as well as their house and tomb based culture. The town is also known for thermal springs because of the large volcanic terrain of the area, and is widely visited by local people for the water therapeutic purpose. Afternoon city tour and visiting the family artistic economic livelihood."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Antsirabe – Ranomafana",
                        content: "The journey is lasting about 8 hours to reach Ranomafana. You pass through the vast agricultural field, which is a dark fertile land making the area as the agricultural potential of the island. On the way, you stop at Ambositra which is the center of wood carving and marquetry. You stay three nights in Ranomafana."
                    }
                ]
            },
            {
                name: "DAY 4-5",
                steps: [
                    {
                        title: "Ranomafana ",
                        content: "Enjoy your two days guided excursion in order to spot rainforest wildlife with your guides. Ranomafana is part of the rainforest considered to be natural world heritage site of UNESCO. The park covers an area of 41 600 ha which contains a mixture of primary and mature secondary forest with mature canopy up to 30 m. The park harbors a great diversity of primates and birds and also different kind faunal communities. In term of lemurs, those are among the target species to be seen in Ranomafana National Park: red bellied lemur, eastern grey bamboo lemur, greater bamboo lemur, red-fronted brown lemur, black-and-white ruffed lemur, Milne-Edward´s sifaka, brown mouse lemur."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Ranomafana – Ambalavao",
                        content: "After breakfast you will head back to the south and explore the capital administrative of the Betsileo ethnic group whom probably are mainly of Indonesian like the Merina .There are even more skilled and industrious as rice farmers and craft man. Up on arrival in Ambalavao, visiting traditional silk and paper making activity. Have lunch then nearby restaurant and free time in the afternoon."
                    }
                ]
            },
            {
                name: "DAY 7-9",
                steps: [
                    {
                        title: "Ambalavao – Isalo",
                        content: "Drive shortly to get to Anja Park to admire the ring tailed lemurs in their granitic inselberg habitat. The park is run by the community based organization and their dedication and effort to protect this small park was one of the reason population of the ring tailed lemurs have increased largely. Several species of endemic succulent plants are the dominant vegetations within the park, which is already a sign of your heading to tropical dry area. After having done the activity, you keep driving southern wards to be in the Bara ethnic group territory. Stay three nights in Isalo."
                    }
                ]
            },
            {
                name: "DAY 10-11",
                steps: [
                    {
                        title: "Isalo National Park",
                        content: "Isalo is one of the largest National Park, it has 81 540 Ha and the most visited protected area because of its quietness and unique geological formation; it is mainly formed by erosional detritus from the basement rock and was chiefly cemented by iron oxide. First day exploring the Namaza canyon, this is the best site to see wildlife and walk through the gorge up to the natural swimming pool. Second day hiking expedition of another site to see the Bara traditional tomb style and learn their culture, this is also an interesting trail for landscape admiring. Dinner at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 12",
                steps: [
                    {
                        title: "Isalo – Toliary",
                        content: "Morning departure to the southern domain and stop at Zombitse National park, which the last remnant transitional forest between the western and southern floristic domain. Zombitse is renowned for its impressive endemic bird community like giant and crested couas and Appert tretraka. You will then continue your drive south to Ifaty. Stay two nights in Ifaty."
                    }
                ]
            },
            {
                name: "DAY 13",
                steps: [
                    {
                        title: "Reniala Reserve",
                        content: "Morning visit of Reniala garden and botanical reserve which have typical vegetation  the spiny forest, and also another site to be familiar with the important of the  Malagasy mangrove  and their conservation effort have been made in the south. Three time in the afternoon."
                    }
                ]

            },
            {
                name: "DAY 14",
                steps: [
                    {
                        title: "Toliary-Antananarivo",
                        content: "Transfer to the airport "
                    }
                ]
            }

        ]
    },

    southwest: {
        images: [
            '/image/baobab/favori.jpg',
            '/image/lemurs/lemur35.jpg',
            '/image/bird/bird2.jpg'
        ],
        title: "Southwest part of Madagascar",
        subtitle: "Baobab avenue – dry deciduous forest – limestone (Tsingy)",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On your arrival, you will be met by our local representative to assist you changing money and reconfirm your domestic flight, after that you will be driven to overnight at Hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Tana - Marofandilia",
                        content: "This morning, you will be transported back to the airport and take the flight to Morondava. Up on arrival, you will be transferred in your 4x4 vehicle to Kirindy Forest about 2 hours on a dirty road passing through the Baobab avenue. Set off on a night walk late in the evening in searching for the world smallest primate and another nocturnal species of lemurs."
                    },
                    {
                        title: "Kirindy Forest",
                        content: "Located in the central Menabe Region of the western part of Madagascar and south of Morondava, the area is characterized by dry deciduous forest which grows on predominately sandy red and yellow soil, most of the plants have an interesting adaptation to cope with prolonged periods of water deficit. Small lives, spines, thickened stems are the common functional adaptive strategies. Kirindy is home to 15 species of amphibians, 45 species of reptiles, 82 species of birds and 35 species of mammals, 8 of which are lemurs. Kirindy forest is one of the largest remaining tracts of dry deciduous forest in Madagascar, and is also an established research station focused on lemur behavior, ecology of 8 species sympatric lemurs."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Morning exploring the Kirindy forest.",
                        content: "The star of the forest is Verreaux’s Sifaka, a red fronted brown lemur, and habituated Fossa hang around the researcher’s camp site and can be seen during the day. Some of the important bird species can be also easily seen such as sakalava weaver, white breasted mesite, and a few Madagascar’s most celebrated endemic family the vangas."
                    }
                ]
            },
            {
                name: "DAY 4",
                steps: [
                    {
                        title: "Marofandilia – Bekopaka",
                        content: "This morning after breakfast, departure to Belo-sur Tsiribihina this is the cultural capital of the sakalava ethnic group, after disembarking from the ferry. You will having lunch at Belo-sur-Tsiribihina restaurant.The journey will be continuing to Bekopaka on a dusty, rough road. You should reach the hotel by 06PM. "
                    }
                ]
            },
            {
                name: "DAY 5-6",
                steps: [
                    {
                        title: "",
                        content: "Over the next two days, you will be taken on a guided excursion in the spectacular limestone formation known as TSINGY in Malagasy. The Tsingy is one of the most well-known parks in Madagascar; and is among the sedimentary rock formation in the island. The Tsingy of Bemaraha was formed by marine origin and is considered to be the oldest group compared to those who found near to the shore. The park contains various ecosystems, including deciduous forest, xerophytic shrublands, sub humid forest, swamps and savanna. The park is important for a range of mammal species, including endemic sportive and dicken’s sifaka, and rich in herpetofaunal species."
                    }
                ]
            },
            {
                name: "DAY 7",
                steps: [
                    {
                        title: "Bekopaka – Morondava",
                        content: "Today start to return the journey by road and crossing the car ferry and keep driving on dusty road back to Morondava. One of the marvelous moments during the travel is passing through the Baobab Avenue. Stop at the baobab Avenue, photograph opportunity of sunset. Spend the night in Morondava hotel."
                    }
                ]
            },
            {
                name: "DAY 8",
                steps: [
                    {
                        title: "Morondava – Antananarivo",
                        content: "Transfer to Morondava airport and flight to Antananarivo"
                    }
                ]
            },
            {
                name: "DAY 9",
                steps: [
                    {
                        title: "Flight back",
                        content: "..."
                    }
                ]
            }
        ]
    },

    east: {
        images: [
            '/image/other/lemurpalmarium.jpg',
            '/image/chameleon/chameleon3.jpg',
            '/image/flora/flora7.jpg',
            '/image/other/palmarium.jpg',
            '/image/other/toamasina.jpg',
            '/image/other/palmarium2.jpg'
        ],
        title: "ANDASIBE - PALMARIUM RESERVE",
        subtitle: "Tropical humid forest and littoral forest – primate watching",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On your arrival, you will be met by our local representative to assist you changing money and help you to different formalities. Overnight at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Antananarivo - Andasibe",
                        content: "This morning, you will depart on the 04H30 drives to Andasibe which is still part of the central domain. The journey enables you to know the people of the central island and knowing their daily economic activities. An introductory night walk. Stay three nights in Andasibe."
                    }
                ]
            },
            {
                name: "DAY 3-4",
                steps: [
                    {
                        title: "Analamazaotra and Mantadia National Parks",
                        content: "This is one of Madagascar’s most popular reserves. Those two parks are typical mid altitude moist mountain forest with altitude 920 to 1100 m where one can explore both secondary and primary forest. The vegetation is evergreen as the area is under the influence of the southeastern trade winds. And the canopy height can reach 25 m. The parks are home to a variety of lemurs, birds, reptiles and invertebrates and different kinds of nicely attracting insects. The main target is to look for the first and second largest living species of lemurs. Spend 2 days is exploring the forest with private local guide, searching for an amazing wildlife which reside here. This is one of the wonderful places in Madagascar that allows you to have a fantastic opportunity to experience some of Madagascar biodiversity species richness."
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "Andasibe – Palmarium reserve",
                        content: "After having explored the evergreen forest of Andasibe, you drive back to the National Road to 2 to reach Manambato village. There you will embark on board a boat leading you to Palmarium. You will navigate along series of lakes linked be artificial canals which developed during French colonial period for commercial use. Different kinds of aquatic vegetation adorn the edge of the canal where wetland bird species can be spotted such as: Madagascar kingfisher, green backed heron … You will spend two nights at Palmarium hotel."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Visiting Palmarium reserve",
                        content: "Morning expedition around the littoral forest of Palmarium protecting a wide variety of important Malagasy vegetation species, particularly ferns, pandanus, and the most well-known endemic pitcher plants, orchids. There are also some species thriving in this reserve such as chameleons, frogs, birds and is one of the few places where lemurs can be encountered close by including the aye-aye. Spend the night at Palmarium hotel and nocturnal excursion before dinner is organized to see Aye-Aye. Dinner at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 7",
                steps: [
                    {
                        title: "Return to Antananarivo",
                        content: "You navigate along the artificial pangalanes canal to join Manambato village, and driven to the capital Antananarivo, en route taken to the Lisy gallery for an overview of all of the handicraft for which Madagascar is known. Dinner and overnight at the hotel."
                    }
                ]
            },
            {
                name: "DAY 8",
                steps: [
                    {
                        title: "Flight back",
                        content: "..."
                    }
                ]
            }

        ]
    },

    birding_tour: {
        images: [
            '/image/bird/favori.jpg',
            '/image/bird/bird2.jpg',
            '/image/bird/bird51.jpg',
            '/image/bird/bird55.jpg',
            '/image/bird/bird52.jpg',
            '/image/bird/bird3.jpg'
        ],
        title: "BIRDING TOUR PROPOSAL SITES",
        subtitle: "Eastern Rain forest Birds",
        days: [
            {
                name: "Low land and littoral forest",
                steps: [
                    {
                        title: "Masoala",
                        content: "Helmet vanga-Bernier’s vanga-Wood Rail-Scaly ground Roller-Short-legged ground roller- red fronted coua- brown mesite –Red tailed newtonia- Red tailed newtonia."
                    }
                ]
            },
            {
                name: "Mid altitude Rainforest",
                steps: [
                    {
                        title: "Analamazoatra-Mantadia-Ranomafana",
                        content: "Nuthatch vanga – Red breasted coua-Collared night jar-long-eared owl-Rainforest scops owl-scaly ground Roller-spiny tailed swift- Grested Ibis- Meller’s duck-little Grebe-Madagascar Rail-Pollen’s vanga – Brown mesite- Grey Emutail-Brown Emutail-Forest Rock thrush-Rufus headed ground Roller- Madagascar spine- yellowbellied sunbird asity-crossley vanga- Pygmy King fisher- Pita like ground roller all Jerry and Newtonia-blue coua-Tretraka"
                    }
                ]
            },
            {
                name: "Transitional forest",
                steps: [
                    {
                        title: "",
                        content: "Appert’s tretraka-Giant coua"
                    }
                ]
            },
            {
                name: "Savanna and wooded grassland",
                steps: [
                    {
                        title: "",
                        content: "Savanna and wooded grassland. Sand grouse-Patrige-Kestrel-Yellow-billed Kite- Madagascar harrier-Buttonquail."
                    }
                ]
            },
            {
                name: "Spiny forest",
                steps: [
                    {
                        title: "",
                        content: "Sub dessert Mesite-long tailed ground roller-green cupped coua - Running coua - Verreaux’s coua - Archbold’s newtonia-Tharmnornis warbler - Banded Kestrel - Lafresnaye’s Vanga - Madagascar Harrier hawk-Sub desert Brush warbler."
                    }
                ]
            },
            {
                name: "Western dry decidious forest",
                steps: [
                    {
                        title: "",
                        content: "White breasted Mesite-Schledgel’s Asity-Vandam’s vanga – Suckle-billed vanga-coqueril’scoua-red capped coua- Scops owl- Rufous vanga- White headed vanga-green pigeon."
                    }
                ]
            },
            {
                name: "Wet land birds",
                steps: [
                    {
                        title: "",
                        content: "Cormorant - Darter - Heron egret - Ducks - flamingo - Rail-Pranticole."
                    }
                ]
            }

        ]
    },

    northwest: {
        images: [
            '/image/landscape/landscape82.jpg',
            '/image/reptile/reptile23.jpg',
            '/image/lemurs/lemur43.jpg',
            '/image/landscape/landscape105.jpg',
            '/image/bird/bird56.jpg',
            '/image/bird/bird52.jpg'
        ],
        title: "Dense forest-Savanna herbs",
        subtitle: "",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Arrival Antananarivo",
                        content: "On your arrival, you will be met by our  local representative to assist you changing money and help you with different procedures . Overnight at the Hotel"
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Antananarivo - Ankarafantsika",
                        content: "After early breakfast, you leave  from the Hotel towards the western part of  the capital, where the vegetation pattern is considered as anthropogenic grassland which is the dominant vegetation in most part of the central area and the western domain. Upon on arrival, you will be able to relax shortly before going to explore the forest to spot some the fauna which are more adaptable to several months of the dry season. Night at the hotel."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Ankarafantsika National Park",
                        content: "his is one of the  most well-known National Park in the island, which as considered a paradise site for naturalists. This site covers an area of 135,000 ha , and  consists of a mosaic dense dry forest and savanna, it is remarkable for its natural habitat and biodiversity diversification. The vegetation is more open and the trail is fairly easy and making the park among the best wildlife site. Ankarafantsika National Park is home to a variety fauna species, such as: lemurs like coquerel’s sifaka, mongoose lemur, and some of the important species of birds such as sickled billed vanga, asity and couas and some wetland bird species near by the park, and different reptiles community such as cuvier’s Madagascar swift, Girdled lizard, and Nil crocodile. Optional nocturnal visit. Night at the Hotel. "
                    }
                ]
            },
            {
                name: "DAY 4",
                steps: [
                    {
                        title: "Ankarafantsika - Mahajanga",
                        content: "After breakfast, morning departure to the western coastal area. En route, you will be able to see the different varieties scenery based sedimentary rock formation toward Mahajanga. Night at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "Mahajanga",
                        content: "Mahajanga is a administrative capital of Sakalava ethnic group and Boeny region, it is situated in the western coastal part of Madagascar. This area received a number of African immigrants from across the Mozambique channel and their influence shows not only the racial characteristics of the people, but there are several customs existing in this area, whose dialect imitates Bantu words, and  African origin. There you will embark on board a boat leading you to Katsepy. It is a fishing village across the bay from Mahajanga. Walk within the Antrema Reserve, it is a largest protect site and wetland forest, including some endemic species of flora and fauna. This reserve constitutes 153 species of plants, which 78% endemicity, 75 species of birds, 24 species of waterbirds, 5 endemic lemur species, 18 species of reptiles. Return back to Mahajanga. Enjoy your afternoon leisure. Night at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Mahajanga-Antananarivo",
                        content: "After breakfast and morning leisure, transfer to the Airport to Antananarivo. Upon arrival you are met with our local representative and to transfer to in your hotel for a day use."
                    }
                ]
            },
            {
                name: "DAY 7",
                steps: [
                    {
                        title: "Antananarivo airport",
                        content: "Flight back home..."
                    }
                ]
            }

        ]
    },

    ambohimanjaka: {
        images: [
            '/image/other/pachi.jpg',
            '/image/geological/geological29.jpg',
            '/image/other/statut_maki.jpg',
            '/image/landscape/landscape67.jpg',
            '/image/landscape/landscape31.jpg',
            '/image/geological/geological7.jpg'
        ],
        title: "Discover Ambohimanjaka differently",
        subtitle: "Trekking tour -  geology - endemic forest - rural life - inselberg vegetation",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Arrival in at Antananarivo",
                        content: "On your arrival in Antananarivo, you will met by our local representative who will assist you changing  money and will help you with the different formalities. After that, you will be chauffeured to the hotel. Dinner and overnight at the hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Tana-Antsirabe",
                        content: "You leave the hotel in the morning and you can stumble across the landscape through the central highland to reach the volcanic area of the island. Along the way, you get to know the highlander people daily economic activities as well as their house and tomb based culture. Stop at Ambatolampy and visit a family factory that manufactures aluminum pots, using a traditional technique before arriving at Antsirabe. Antsirabe is well-known for thermal springs because of the large volcanic terrain of the area, and is widely visited by local people for the water therapeutic purpose. Afternoon city tour and visiting the family artistic economic livelihood. Dinner at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Antsirabe-Ambohimanjaka",
                        content: "after the early breakfast, head to the Ambohimanjaka trail. This region is located in the central highland which makes it possible to admire  various  landscapes of the central plateau, and getting to know the geological formation of the region which is part of the backbone of the island before climbing up to the peak on a system of paths, at a higher altitude of 2000 m. You can explore  the endemic Tapia woodland forest and learn the importance of ecosystem benefits it provides, there are also interesting succulent  plants species to be seen like aloe and pachypodium.The waterfall  and the Gallery forest are an additional component of the hike. Enjoying the panoramic view of the surrounding village  at the top of Ankarana. You can appreciate as well the amazing granitic rock formation of the area resembling to tsingy. Return to Ambohimanjaka. Dinner and overnight stay at a local. "
                    }
                ]
            },
            {
                name: "DAY 4",
                steps: [
                    {
                        title: "Antsirabe - Antananarivo",
                        content: "fter breakfast, return back to Antananarivo."
                    }
                ]
            }
        ]
    },

    wildlife_watching: {
        images: [
            '/image/lemurs/lemur12.jpg',
            '/image/chameleon/chameleon3.jpg',
            '/image/flora/flora7.jpg'
        ],
        title: "WILDLIFE WATCHING TOUR",
        subtitle: "DEEP SOUTH SPINY FOREST and GALLERY FOREST - CENTRAL RAINFOREST",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "ARRIVAL IN ANTANANARIVO",
                        content: "On arrival in Antananarivo you will be met by our local representative to assist you changing money and reconfirm your domestic flight, after that you will be driven to Overnight in the city hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "ANTANANARIVO - TAOLAGNARO - BERENTY",
                        content: "Based on the schedule, you will be transfered back to the airport, and take the flight to Taolagnaro. Upon arrival, depart to Berenty for 4 hours’ drive on dusty pist road. Admiring the vegetation change from tropical forest to transitional and spiny forest as well as the typical Antanosy ethic group memorial stone are the highlight of landscape viewing along the road. Overnight at Berenty Reserve for 3 nights."
                    }
                ]
            },
            {
                name: "DAY 3-4",
                steps: [
                    {
                        title: "BERENTY RESERVE",
                        content: "You will be taken on guided walk to encounter an amazing wildlife inside the gallery and spiny forest, like ring tailed lemur, the dancing lemur, and some of Madagascar endemic bird’s community such as crested and giant couas, as well as some of Madagascar’s most celebrated endemic family vangas, and seeking the large colonies of Madagascar flying fox in their roosting tree. Night walk in the spiny and gallery forest."
                    }
                ]
            },
            {
                name: "DAY 5",
                steps: [
                    {
                        title: "BERENTY – ANTANANARIVO",
                        content: "After breakfast, you come back to Taolagnaro to take the flight back to Antananarivo. Overnight in the city hotel."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "ANTANANARIVO - ANDASIBE",
                        content: "After a leisurely morning, you take a beautiful 4h30 drive to Andasibe area. The drive to the east part of the city is on a paved road through the high plateau, passing rice field, mountains, and learning the highlander culture and custom and their daily economic activity. Introductory night walk. Overnight at hotel."
                    }
                ]
            },
            {
                name: "DAY 7-8",
                steps: [
                    {
                        title: "ANALAMAZAOTRA NATIONAL PARK",
                        content: "This is one of Madagascar’s most popular reserves. This park is typical mid altitude moist mountain forest with altitude 920 to 1100 m where one can explore both secondary and primary forest. The vegetation is evergreen as the area is under the influence of the southeastern trade winds. And the canopy height can reach 25 m. The park is home to a variety of lemurs, birds, reptiles and invertebrates and different kinds of nicely attracting insects. Primate watching that is the first and second largest species of lemurs is among the highlight of this area. Spend 2 days including night walks in your exploring the rainforest with private local guide searching for an amazing wildlife which reside here. This is one of the wonderful places in Madagascar that allows you to have a fantastic opportunity to experience some of Madagascar biodiversity species richness."
                    }
                ]
            },
            {
                name: "DAY 9",
                steps: [
                    {
                        title: "Flight back",
                        content: "After early breakfast, you return back to Antananarivo to get your afternoon flight-out or day use at a hotel nearby the Airport for your early morning flight. End of our service."
                    }
                ]
            }

        ]
    },

    southeast: {
        images: [
            '/image/other/mongrove_manakara.jpg',
            '/image/other/mongrove_manakara1.jpg',
            '/image/landscape/landscapeM1.jpg',
            '/image/lemurs/lemur117.jpg',
            '/image/lemurs/lemur63.jpg',
            '/image/landscape/landscape11.jpg'
        ],
        title: "SOUTHEAST - MANAKARA",
        subtitle: "",
        days: [
            {
                name: "DAY 1",
                steps: [
                    {
                        title: "Antananarivo",
                        content: "On your arrival, you will be met by our local representative to assist you changing money and after that you will be driven to overnight at Hotel."
                    }
                ]
            },
            {
                name: "DAY 2",
                steps: [
                    {
                        title: "Tana-Antsirabe",
                        content: "After early breakfast,  you can admire the landscape through the central highland to reach the volcanic area of the island. En route, you get to know the highlander people daily economic activities as well as their house and tomb based culture. The town is also known for thermal springs because of the large volcanic terrain of the area, and is widely visited by local people for the water therapeutic purpose. Afternoon city tour and visiting the family artistic economic livelihood."
                    }
                ]
            },
            {
                name: "DAY 3",
                steps: [
                    {
                        title: "Antsirabe-Ranomafana",
                        content: "The journey is lasting about 8 hours to reach Ranomafana. You pass through the vast agricultural field, which is a dark fertile land making the area as the agricultural potential of the island. Along the way, you stop at Ambositra which is the center of wood carving and marquetry. You stay three nights in Ranomafana."
                    }
                ]
            },
            {
                name: "DAY 4-5",
                steps: [
                    {
                        title: "Ranomafana",
                        content: "Enjoy your two days guided excursion in order to spot rainforest wildlife with your guides. Ranomafana is part of the rainforest considered to be natural world heritage site of UNESCO. The park covers an area of 41 600 ha which contains a mixture of primary and mature secondary forest with mature canopy up to 30 m. The park harbors a great diversity of primates and birds and also different kind faunal communities. In term of lemurs, those are among the target species to be seen in Ranomafana National Park: red bellied lemur, eastern grey bamboo lemur, greater bamboo lemur, red-fronted brown lemur, black-and-white ruffed lemur, Milne-Edward´s sifaka, brown mouse lemur."
                    }
                ]
            },
            {
                name: "DAY 6",
                steps: [
                    {
                        title: "Ranomafana-Manakara",
                        content: "Departure in the morning to the south-eastern coastal area of Madagascar, passing through the Tanala typical village who are known to be people of the forest, their traditional house is made mainly with vegetal  materials,  and rely heavily on the natural resources around them for their livelihood,several important cash crops are growing in the area and play very important role in region’s. Upon on arrival, you will be able to relax. Night and dinner at Hotel."
                    }
                ]
            },
            {
                name: "DAY 7",
                steps: [
                    {
                        title: "Manakara",
                        content: "Manakara is an ancient colonial city located on the south-eastern coast of Madagascar, capital of the vatovavy fitovinany region. This city is dominated by the Antehimoro people and well known in the artisanal manufacture of Antehimoro paper. It is an area shaded by Filao trees, surrounding by luxuriant green vegetation and unique  coastal of diversity, which this forest harbor a very unusual  community. You will go shortly on a canoe  to adventure on the Pangalanne canal. It is a  the largest canal in  the eastern  of Madagascar and was created in colonial periods.  This canal interconnected the natural rivers and lagoons,  and was very important for economic reason during the  colonization era, which along nearly 650 km and flow to the Indian Ocean sea. You will visit  small villages along the shore to discover the daily life of the villagers mainly focusing on fishing, but also the weaving of mats and the construction of canoes. City visit, you will explore the  different ancient colonial buildings with  their history, visiting Manakara train station with their French architectural influence.  Night at the Hotel."
                    }
                ]
            },
            {
                name: "DAY 8",
                steps: [
                    {
                        title: "Manakara-Antananarivo",
                        content: "After breakfast and morning leisure, Return  to Antananarivo. Upon arrival you are met with our local representative and to transfer to in your hotel for a day use. "
                    }
                ]
            }

        ]
    },
}

export default data;

