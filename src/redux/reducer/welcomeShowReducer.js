const welcommeShowReducer = (state = false, action) => {
    if(action.type === "WELCOME_SHOWED") {
        return action.payload;
    } else {
        return state;
    }
}

export default welcommeShowReducer;