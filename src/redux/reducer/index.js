import welcomeShowed from "../reducer/welcomeShowReducer";

import { combineReducers } from "redux";

const allReducers = combineReducers({
    welcomeShowed
});

export default allReducers;