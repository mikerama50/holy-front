export const welcomeShowed = (showed) => {
    return {
        type: "WELCOME_SHOWED",
        payload: showed
    }
}