import React, { useEffect, useState } from "react";
import data from "../../data/dataGallery";
import "../../assets/css/gallery.css";

export default function Gallery() {

    const [imageGrid, setImageGrid] = useState(null)

    useEffect(() => {
        setImageGrid(
            <div className="w3-row-padding">
                {data.map((item, key) => (
                    <div key={key} className="w3-col l3 m4 s6 w3-margin-bottom">
                        <div className="flip-box w3-round-xlarge">
                            <a href={"/detail/" + item.id + "?name=" + item.name}>
                                <div className="flip-box-inner w3-round-xlarge">
                                    <div className="flip-box-front w3-round-xlarge">
                                        <img src={item.image} className="imageGallery w3-round-xlarge" alt={item.name} />
                                    </div>
                                    <div className="flip-box-back w3-round-xlarge w3-display-container">
                                        <img src={item.image} className="imageGallery w3-round-xlarge" alt={item.name} />
                                        <div className="w3-black w3-round-xlarge w3-opacity w3-block w3-display-middle" style={{ height: "100%" }}></div>
                                        <div className="w3-display-middle w3-block w3-padding">
                                            <div className="w3-text-white textNameImageGallery w3-block">{item.name}</div>
                                            <div className="w3-block w3-margin descriptionGallery">{item.description}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                ))
                }
            </div>
        )
    }, []);

    return (
        <>
            {imageGrid}
        </>
    );
}