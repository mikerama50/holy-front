import React from "react";
import queryString from "query-string";
import { LazyLoadImage } from "react-lazy-load-image-component";
import data from "../../data/dataDetails";
import "../../assets/css/detail.css";
import "../../assets/css/welcome_page.css";
import $ from "jquery";


export default class Detail extends React.Component {

    state = {
        categorie: "",
        favori: "",
        name: "",
        description: "",
        imageGallery: [],
        localisation: "",

    }

    viewMore = (localisation) => {
        alert(localisation);
        window.location.assign("/detail/localisation/" + localisation);
    }

    buildGalleryImage = () => {
        const categorie = this.props.match.params.categorie;
        const imagessByLocalisation = data[categorie].imagesByLocalisation;
        const localisationGallery = [];
        for (var i = 0; i < imagessByLocalisation.length; i++) {
            var localisation = imagessByLocalisation[i].localisation;
            var images = [];
            var imagesHide = [];

            for (var j = 0; j < imagessByLocalisation[i].species.length; j++) {
                if (imagessByLocalisation[i].species.length < 13) {
                    images.push(
                        <div key={j} className="w3-col s6 m4 l4 w3-margin-bottom w3-display-container" style={{ height: 200 }}>
                            <a href={"/detail/localisation/" + localisation} className="w3-display-topright view_more_btn">view more</a>
                            <a href={"/detail/localisation/" + localisation}> 
                                <div className="imageContainerDetail w3-round-xlarge">
                                    <LazyLoadImage src={imagessByLocalisation[i].species[j].image} className="w3-imageDetail w3-round-xlarge w3-block w3-height" alt={imagessByLocalisation[i].species[j].name} />
                                    <h1 className="w3-display-bottomleft nameImage">{imagessByLocalisation[i].species[j].name}</h1>
                                </div>
                            </a>
                        </div>
                    )
                } else {
                    if (j < 12) {
                        images.push(
                            <div key={j} className="w3-col s6 m4 l4 w3-margin-bottom w3-display-container" style={{ height: 200 }}>
                            <a href={"/detail/localisation/" + localisation} className="w3-display-topright view_more_btn">view more</a>
                                <a href={"/detail/localisation/" + localisation}>
                                    <div className="imageContainerDetail w3-round-xlarge">
                                        <LazyLoadImage src={imagessByLocalisation[i].species[j].image} className="w3-imageDetail w3-round-xlarge w3-block w3-height" alt={imagessByLocalisation[i].species[j].name} />
                                        <h1 className="w3-display-bottomleft nameImage">{imagessByLocalisation[i].species[j].name}</h1>
                                    </div>
                                </a>
                            </div>
                        )
                    } else {
                        imagesHide.push(
                            <div key={j} className="w3-col s6 m4 l4 w3-margin-bottom w3-display-container" style={{ height: 200 }}>
                            <a href={"/detail/localisation/" + localisation} className="w3-display-topright view_more_btn">view more</a>
                                <a href={"/detail/localisation/" + localisation}>
                                    <div className="imageContainerDetail w3-round-xlarge">
                                        <LazyLoadImage src={imagessByLocalisation[i].species[j].image} className="w3-imageDetail w3-round-xlarge w3-block w3-height" alt={imagessByLocalisation[i].species[j].name} />
                                        <h1 className="w3-display-bottomleft nameImage">{imagessByLocalisation[i].species[j].name}</h1>
                                    </div>
                                </a>
                            </div>
                        )
                    }

                }

            }
            if (imagessByLocalisation[i].species.length < 13) {
                localisationGallery.push(
                    <div key={i} className="w3-content">
                        <h1 className="w3-margin-left titleLocalisation w3-text-red">{imagessByLocalisation[i].localisation}</h1>
                        <div className="w3-row-padding w3-margin-bottom w3-margin-top w3-margin-left">
                            {images.map(item => (item))}
                        </div>
                        <div className="w3-row-padding">
                            <div className="w3-col l12"><p className="grey">What you see does not sum up the richness and beauty of this region in terms of biodiversity and culture, there is still more that you can discover through this site.</p></div>
                        </div>
                        <div className="w3-row-padding"><hr /></div>
                    </div>
                )
            } else {
                localisationGallery.push(
                    <div key={i} className="w3-content">
                        <h1 className="w3-margin-left titleLocalisation w3-text-red">{imagessByLocalisation[i].localisation}</h1>
                        <div className="div1 w3-row-padding w3-margin-bottom w3-margin-top w3-margin-left">
                            {images.map(item => (item))}
                        </div>

                        <div id={"showImage" + i} className="div2 w3-row-padding w3-margin-bottom w3-margin-top w3-margin-left w3-hide">
                            {imagesHide.map(item => (item))}
                        </div>
                        <div className="w3-row-padding">
                            <div id={i} className="hideMore w3-xlarge w3-text-white w3-teal w3-round-large w3-margin-left w3-margin-bottom w3-card w3-padding w3-center">View more photos</div>
                            <div className="w3-col l12"><p className="grey">What you see does not sum up the richness and beauty of this region in terms of biodiversity and culture, there is still more that you can discover through this site.</p></div>
                        </div>
                        <div className="w3-row-padding"><hr /></div>
                    </div>
                )
            }

        }
        this.setState({
            imageGallery: localisationGallery
        })
    }

    componentWillMount() {
        const name = queryString.parse(this.props.location.search);
        const categorie = this.props.match.params.categorie;
        if (data.hasOwnProperty(categorie)) {
            this.setState({
                favori: data[categorie].favori,
                name: name.name,
                description: data[categorie].description
            });
            this.buildGalleryImage();
        } else {
            window.location.assign("/home/noWelcome");
        }
        $(document).ready(() => {
            setTimeout(() => {
                $(".flip-face").css({ "display": "none" });
                $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" });
                $(".flip-box-well").css({ "position": "static" });
            }, 1000)

            $(".hideMore").click(function () {
                if (this.innerText === "Hide") {
                    $("#showImage" + this.id).attr({ "class": "div2 w3-row-padding w3-margin-bottom w3-margin-top w3-margin-left w3-hide" });
                    this.innerText = "View more photos";
                } else {
                    $("#showImage" + this.id).attr({ "class": "div2 w3-row-padding w3-margin-bottom w3-margin-top w3-margin-left" });
                    this.innerText = "Hide";
                }
            })
        });
    }

    render() {
        return (
            <>
                <div className="flip-box-well w3-block">
                    <div className="w3-block w3-height w3-white w3-display-container flip-box-inner-well">
                        <div className="flip-face">

                        </div>
                        <div className="flip-box-back-well w3-block w3-height w3-white">
                            <div className="w3-block w3-display-container" style={{ height: 400 }}>
                                <img src={this.state.favori} alt={this.state.name} className="w3-block imageFavori" />
                                <div className="w3-block filter w3-display-middle"></div>
                                <h3 className="w3-display-topleft categorieImage">{this.state.name}</h3>
                            </div>
                            {this.state.description !== "" && (
                                <div className="descriptionContainer w3-content w3-row-padding">
                                    <div className="w3-panel w3-leftbar w3-border-red">
                                        <h2 className="amber w3-xxlarge">{this.state.name}</h2>
                                        <p className="grey w3-large w3-margin-left">{this.state.description}</p>
                                    </div>
                                </div>
                            )}
                            {
                                this.state.imageGallery.map(item => (item))
                            }
                        </div>
                    </div>
                </div>
            </>
        );
    }

}
