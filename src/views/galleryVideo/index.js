import React from 'react';
import data from '../../data/dataDetails';
import { LazyLoadImage } from "react-lazy-load-image-component";
import "../../assets/css/galleryVideo.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import $ from 'jquery'

const categories = ['lemur', 'landscape', 'flora', 'geological', 'bird', 'cultural', 'handicraft', 'tortoise', 'reptile', 'amphibian', 'insect', 'chameleon'];

export default class GalleryVideo extends React.Component {

    state = {
        gallery: []
    }

    componentWillMount() {
        
        const categorieContent = [];
        var k=0;
        for (const categorie of categories) {
            const dataCategorie = data[categorie].imagesByLocalisation;
            const categorieLocalisationContent = [];
            for (var i = 0; i < dataCategorie.length; i++) {
                const species = dataCategorie[i].species;
                const speciesContent = [];
                for (var j = 0; j < species.length; j++) {
                    if (species[j].video && species[j].video !== "") {
                        var kilasy = "w3-display-middle w3-red w3-text-white ImageByLocVideo w3-round-xlarge w3-hover-grey w3-padding";
                        speciesContent.push(
                            <div key={j} className="w3-col s6 m4 l3 w3-margin-bottom">
                                <div className="imageContainerGalleryVideo w3-display-container">
                                    <LazyLoadImage className="imageGalleryVideo w3-round-xlarge" src={species[j].image} alt={species[j].name ? species[j].name : "image"} />
                                    <div id={k=k+1} className={kilasy + " clickMe"} title="Click to show video"><FontAwesomeIcon icon={faPlay} /></div>
                                    <div className="w3-display-bottomright w3-teal w3-margin w3-text-white ImageByLoc w3-round-xxlarge w3-large">Loc: {species[j].loc}</div>
                                    <div className="w3-hide" id={"Me" + k}>{species[j].video}</div>
                                </div>
                            </div>
                        )
                    }
                }
                categorieLocalisationContent.push(
                    <>
                        {
                            speciesContent.map(item => (item))
                        }
                    </>
                )
            }
            categorieContent.push(
                <div key={categorie} className="w3-row-padding">
                    <div className="w3-col l12 m12 s12 w3-margin-top JuneGull"><h2 className="h">{(categorie === "cultural" ? "VARIOUS INTERESTING ASPECT" : categorie.toLocaleUpperCase())}</h2></div>
                    {
                        categorieLocalisationContent.map(item => (item))
                    }
                </div>
            )
        }
        $(document).ready(() => {
            $('.clickMe').click(function () {
                var idf = "Me" + this.id;
                var url = document.getElementById(idf).innerHTML;

                if (url.length > 7) {
                    $("#videoContainer").attr({ "src": url });
                    $("#id01").css({ "display": "block" });
                } else {
                    alert("No video found, Comingsoon...");
                }
            });

            $('.close').click(function () {
                $("#videoContainer").attr({ "src": "" });
                $("#id01").css({ "display": "none" });
            });

            $('.clickMyImage').click(function () {
                $(".BigView").html(this.innerHTML);
                $("#id02").css({ "display": "block" });
            });

            setTimeout(() => {
                $(".flip-face").css({ "display": "none" });
                $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" });
                $(".flip-box-well").css({ "position": "static" });
            }, 1000)
        });
        this.setState({
            gallery: categorieContent
        });
    }

    render() {
        return (
            <div className="galleryContainer">
                <div id="id01" className="w3-modal modal">
                    <div className="w3-modal-content  w3-round-xxlarge">
                        <div className="w3-container">
                            <span className="close w3-red w3-padding w3-margin w3-round-xxlarge w3-display-topright videoItinerary">&times;</span>
                            <div style={{ height: 500 }}>
                                <iframe src="" id="videoContainer" title="video" className="w3-round-xlarge w3-red w3-block videoItinerary w3-height" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.gallery.map(item => (item))
                }
            </div>
        )
    }
}