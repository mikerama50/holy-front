import React, { useEffect } from "react";
import data from "../../data/dataDetails";
import Swiper from 'swiper/bundle';
import $ from "jquery";
import 'swiper/swiper-bundle.css';
import '../../assets/css/Carousel.css'

export default function Carousel() {

    useEffect(() => {
        $(document).ready(() => {
            var swiper = new Swiper('.swiper-container',
                {
                    speed: 1000,
                    direction: 'horizontal',
                    navigation:
                    {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    autoplay:
                    {
                        delay: 3000,
                    },
                    loop: true,
                });
        });
    }, []);

    return (
        <div className="w3-block">
            <div className="swiper-container">
                <div className="swiper-wrapper">
                    {
                        data.landscape.imagesByLocalisation[0].species.map((item, key) => (
                            <div key={key} className="swiper-slide w3-block w3-display-container" style={{ height: 600 }}>
                                <img src={item.image} alt={item.name} className="w3-block w3-display-middle imageSlide" />
                                <div className="w3-display-left descriptionContentCarousel">
                                    <h1 className="titleSlide">{(item.carousel ? item.carousel.title : "")}</h1>
                                    <p className="descriptionCarousel">{(item.carousel ? item.carousel.description : "")}</p>
                                    <a href={"/detail/localisation/" + data.landscape.imagesByLocalisation[0].localisation} className="w3-left btn-carousel">View more</a>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <div className="swiper-button-next"></div>
                <div className="swiper-button-prev"></div>
            </div>
        </div>
    )
}