import React from "react";
import "../../assets/css/holyExperience.css"
import { LazyLoadImage } from "react-lazy-load-image-component";

export default function HolyExperience() {
    return (
        <>
            <div className="presentationHolyContainer">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-5 col-lg-5" style={{ paddingTop: 17, marginBottom: 40 }}>
                            <LazyLoadImage src="/image/gallery/team.jpg" style={{ width: "100%", height: 300, objectFit: "cover",borderRadius: 10 }} alt="members" />
                        </div>
                        <div className="col-sm-12 col-md-7 col-lg-7"><h1 style={{ color: "black" }}>About us</h1>
                            <p>Holy Destination Madagascar is an inbound Tour Operator created in 2008  located in Antananarivo Madagascar. We organize different tours throughout Madagascar, the big island. Tailor-made tours, package tours, honeymoons, hiking, themed tours for individuals or groups are our products offered in a wide range for all types of customers. Composed of a young, dynamic team who love their country, we invite you to discover Madagascar, the multi-faceted nature island.
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}