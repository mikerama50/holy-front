import React from "react";
import Gallery from "../gallery";
import SectionTours from "./SectionTours";
import SectionCards from "./SectionCard";
import $ from "jquery";
import "../../assets/css/gallery.css"
import "../../assets/css/welcome_page.css";
import "../../assets/css/animate.min.css";
import "animate.css";
import Welcome from "../welcome_page";
import Carousel from "./Carousel";

export default class Home extends React.Component {

    state = {
        noWelcome: <Welcome /> 
    }

    componentDidMount() {
        $(document).ready(() => {
            var str = window.location.toString();
            var n = "";
            if (str.match("noWelcome")) {
                n = str.match("noWelcome").toString();
            }

            window.addEventListener("resize", function () {
                var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                $(".flip-box-well").css({ "width": w + "px" })
            });
            if (n === "noWelcome") {
                this.setState({
                    noWelcome: <div></div>
                })
                setTimeout(() => {
                    $(".flip-box-well").css({ "position": "static" })
                    $(".flip-face").css({ "display": "none" })
                    $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" })
                }, 1000)
            } else {
                $(".flip-box-well").css({ "position": "fixed" })
                setTimeout(() => {
                    $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" })
                    $(".flip-box-well").css({ "position": "absolute" })
                    setTimeout(() => {
                        $(".flip-face").css({ "display": "none" })
                        $(".flip-box-well").css({ "position": "static" })
                    }, 1000)

                }, 10000);
            }
        });
    }

    render() {
        return (
            <>
                <div class="flip-box-well w3-block">
                    <div className="w3-block w3-height w3-white w3-display-container flip-box-inner-well">
                        <div className="flip-face">
                            {this.state.noWelcome}
                        </div>
                        <div class="flip-box-back-well w3-center w3-block w3-height w3-white">
                            <Gallery />
                            <Carousel />
                            <SectionTours />
                            <SectionCards />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}