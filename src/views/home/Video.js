import React from "react";
import "../../assets/css/circuit.css";


export default function Video() {
    return (
        <div className="w3-container" style={{ marginTop: 65, marginBottom: 36, borderRadius: 4, overflow: "hidden" }}>
            <div className="w3-container w3-half">
                <iframe width="100%" style={{ borderRadius: 5 }} height="350" title="baobab" src="https://www.youtube.com/embed/hMugu3Eu5bw" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
            <div className="w3-container w3-half">
                <iframe width="100%" style={{ borderRadius: 5 }} height="350" title="baobab" src="https://www.youtube.com/embed/vO-_nhRorJI" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
        </div>
    )
}