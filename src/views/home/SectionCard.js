import React from "react";
import "../../assets/css/sectionCard.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { AnimatedOnScroll } from "react-animated-css-onscroll";

export default function SectionCard() {
    return (
        <div id="sectionCard" style={{ marginTop: 65, marginBottom: 40 }}>
            <div style={{ width: "100%", textAlign: "center", marginBottom: 40 }}>
                <div className="w3-margin w3-padding">
                    <h1 className="myMadagascarPresentaion w3-margin-top" style={{ fontSize: 30, color: "rgb(255, 40, 40)", fontFamily: "Ubuntu" }}>Madagascar is a land with particular and wonderful creature</h1>
                    <h1 className="myMadagascarPresentaion">Holydestination offers you:</h1>
                </div>
            </div>
            <div className="cardContainer container w3-row-padding">
                <AnimatedOnScroll animationIn="slideInLeft" className="w3-third">
                    <div className="w3-card-4 w3-round-xlarge my-card" onClick={() => window.location.assign("/offers/FauneFlore")} style={{ width: "100%", overflow: "hidden" }}>
                        <LazyLoadImage src="/image/gallery/FAUNE ET FLORE.JPG" alt="maki catta" className="imgCard" style={{ width: "100%" }} />
                        <div className="w3-container myCardTextContainer">
                            <p className="myCardText color">A place where evolution uniqueness was occurring</p>
                        </div>
                    </div>
                </AnimatedOnScroll>
                <AnimatedOnScroll animationIn="fadeIn" className="w3-third">
                    <div className="w3-card-4 w3-round-xlarge my-card" onClick={() => window.location.assign("/offers/PicturesqueHabitat")} style={{ width: "100%", overflow: "hidden" }}>
                        <LazyLoadImage src="/image/gallery/HABITAT.png" alt="maki catta" className="imgCard" style={{ width: "100%" }} />
                        <div className="w3-container myCardTextContainer" >
                            <p className="myCardText color">Picturesque based landscapes experiencing</p>
                        </div>
                    </div>
                </AnimatedOnScroll>
                <AnimatedOnScroll animationIn="slideInRight" className="w3-third">
                    <div className="w3-card-4 w3-round-xlarge my-card" onClick={() => window.location.assign("/offers/AmazingCulture")} style={{ width: "100%", overflow: "hidden" }}>
                        <LazyLoadImage src="/image/gallery/art.png" alt="maki catta" className="imgCard" style={{ width: "100%" }} />
                        <div className="w3-container myCardTextContainer" >
                            <p className="myCardText color">Various interesting cultural aspect of welcoming people.</p>
                        </div>
                    </div>
                </AnimatedOnScroll>
            </div>
            <div className="my_parallax2 w3-display-container">
                <div className="w3-display-middle w3-block travel_dream_bg">
                    <div className="w3-display-middle travel_dream w3-half w3-container"></div>
                    <div className="travelText w3-display-middle w3-block w3-center">HOLY DESTINATION</div>
                </div>
            </div>
        </div>
    )
}