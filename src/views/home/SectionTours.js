import React, { useEffect, useState } from "react";
import "../../assets/css/sectionTours.css";
import { AnimatedOnScroll } from "react-animated-css-onscroll";

const dataTour = [];

export default function SectionCard() {
    const [tour, setTour] = useState([]);
    const buildTourCategorie = () => {

        const fromPosition = ["Left", "Right"];
        var i = 1;
        setTour(
            dataTour.map(item =>
                <AnimatedOnScroll key={i} animationIn={"slideIn" + fromPosition[(i = i + 1) % 2]} className="w3-half w3-margin-bottom w3-padding">
                    <div className="my-card  w3-light-grey" style={{ width: "100%", overflow: "hidden", height: 200 }}>
                        <div className="w3-container myCardTextContainer w3-block">
                            <h4 className="timeline-title tourText">{item.designation}</h4>
                            <p className="myCardText color">{item.description}</p>
                        </div>
                    </div>
                </AnimatedOnScroll>
            )
        )

    }

    useEffect(() => {
        buildTourCategorie();
    }, [])
    return (
        <div id="sectionCard" style={{ marginTop: 8, marginBottom: 40 }}>
            <div className="cardContainer container w3-row-padding">
                {tour}
            </div>
            <div style={{ width: "100%", textAlign: "center", marginBottom: 10 }}>
                <div className="w3-margin w3-padding">
                    <h1 className="myMadagascarPresentaion w3-margin-top">OVERVIEW</h1>
                </div>
            </div>
            <div className="w3-large color container" style={{ width: "100%", textAlign: "justify", marginBottom: 40 }}>
                <AnimatedOnScroll animationIn="fadeInUp" className="w3-margin-left">
                    Madagascar is regarded as one of the most important areas for biodiversity on earth. With its large size 587000 km2 varied topography, offers a diverse array of habitats occupied by a highly endemic and species of fauna. Madagascar is divided in two major phytogeographic regions, the eastern part generally covered by tropical humid forest, and western region by tropical dry forest. Eastern and western region are even further subdivided into several domains.
                    It lies approximately 400km to the east of Africa at narrowest point of the Mozambique Channel and it is other wide completely isolated from significant landmasses.
                    Its geological history geographic isolation has conspired to create its unique assemblage of organisms.
                    Madagascar flora and fauna are diverse and unique, due to the island’s ancient isolation, and also of the complexity of its topography and ecology.
                    Owing to its large surface areas, and its various assortments of microclimates and habitats, it is considered as a mini continent.
                </AnimatedOnScroll>
            </div>
            <div className="my_parallax w3-display-container">
                <div className="w3-display-middle w3-block travel_dream_bg">
                    <div className="w3-display-middle travel_dream w3-half w3-container"></div>
                    <div className="travelText w3-display-middle w3-block w3-center">MADAGASCAR</div>
                </div>
            </div>
        </div>
    )
}