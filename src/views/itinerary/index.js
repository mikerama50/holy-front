import React from "react";
import dataItinerary from "../../data/dataItinerary";
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import "../../assets/css/itinerary.css";
import "../../assets/css/welcome_page.css";
import $ from 'jquery'

export default class Itinerary extends React.Component {

    state = {
        title: "",
        subtitle: "",
        circuitContent: [],
        imageContainer: null
    }

    buildItinerary = () => {
        const nameCircuit = this.props.match.params.name;
        const circuit = dataItinerary[nameCircuit];
        this.setState({
            title: circuit.title,
            subtitle: circuit.subtitle
        });
        var circuitContent = [];
        for (var i = 0; i < circuit.days.length; i++) {
            var stepsContent = circuit.days[i].steps;
            var dayContent = [];
            for (var j = 0; j < stepsContent.length; j++) {
                dayContent.push(
                    <div key={j}>
                        <h4 className="vertical-timeline-element-subtitle w3-text-teal">{stepsContent[j].title}</h4>
                        <p>{stepsContent[j].content}</p>
                    </div>
                )
            }
            circuitContent.push(
                <VerticalTimelineElement
                    key={i}
                    className="vertical-timeline-element--work"
                    contentStyle={{ background: 'white' }}
                    contentArrowStyle={{ borderRight: '7px solid  white' }}
                    iconStyle={{ background: '#009688', color: '#fff' }}
                >
                    <h3 className="vertical-timeline-element-title w3-text-red JuneGull w3-xlarge">{circuit.days[i].name}</h3>
                    {
                        dayContent.map(item => (item))
                    }
                </VerticalTimelineElement>
            )
        }
        this.setState({
            circuitContent: circuitContent
        });
    }

    buildImage = () => {
        const nameCircuit = this.props.match.params.name;
        const images = dataItinerary[nameCircuit].images;
        this.setState({
            imageContainer: (
                <div className="w3-row-padding">
                    {
                        images.map((item, key)=> (
                            <div key={key} className="w3-col l4 w3-margin-bottom w3-round-xlarge">
                                <img src={item} alt="" className="imageItinerary w3-round-xlarge" />
                            </div>
                        ))
                    }
                </div>
            )
        })
    }

    componentWillMount() {
        $(document).ready(() => {
            setTimeout(() => {
                $(".flip-face").css({ "display": "none" });
                $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" });
                $(".flip-box-well").css({ "position": "static" });
            }, 1000)
        });
        this.buildItinerary();
        this.buildImage();
    }

    render() {
        return (
            <>
                <div class="flip-box-well w3-block">
                    <div className="w3-block w3-height w3-white w3-display-container flip-box-inner-well">
                        <div className="flip-face">

                        </div>

                        <div class="flip-box-back-well w3-center w3-block w3-height w3-white">
                            <div className="w3-container">
                                <div className="headerItinerary">
                                    <h1 className="title w3-center JuneGull w3-xxxlarge w3-card w3-text-teal">{this.state.title}</h1>
                                    <h2 className="subtitle w3-center JuneGull w3-text-grey w3-xlarge">{this.state.subtitle}</h2>
                                    <div className="imageContainerItinerary">
                                    </div>
                                </div>
                                {this.state.imageContainer}
                                <div className="bodyItinerary w3-content">
                                    <VerticalTimeline>
                                        {
                                            this.state.circuitContent.map(item => (item))
                                        }
                                    </VerticalTimeline>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>

        );
    }
}