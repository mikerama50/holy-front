import React, { useState, useEffect } from "react";
import Offers from "./dataHabitat";
import "../../assets/css/offers.css";
import { LazyLoadImage } from "react-lazy-load-image-component";

export default function () {

    const [offers, setOffers] = useState([])
    

    useEffect(() => {
        const position = ["left", "right"]
        var i = 1
        setOffers(
            Offers.off.map(
                item => (

                    <div className="w3-container w3-row">
                        <div className={"w3-col col-sm-12 col-md-7 col-lg-7 w3-display-container w3-" + position[(i = i + 1) % 2] + ""}>
                            <LazyLoadImage src={item.image} alt={item.Name} className="imageFauneAndFlore" />
                            <LazyLoadImage src={"/image/gallery/" + position[(i + 1) % 2] + ".png"} alt="blur" className={"w3-display-top" + position[(i + 1) % 2] + " deco"} />
                            <div className="w3-container w3-block firstLayers w3-display-middle"> </div>
                        </div>
                        <div className="w3-col col-sm-12 col-md-5 col-lg-5">
                            <p className="line"> </p>
                            <p className="myKind"><span className="myKindChild1">{item.titlePart1}</span><br /><span className="myKindChild2">{item.titlePart2}</span></p>
                            <p className="myParagraphFaune_flore color">{item.description}</p>
                        </div>
                    </div>
                )
            )
        )

    }, [])
    return (
        <div>
            <div className="w3-display-container offers">
                <LazyLoadImage src="/image/gallery/HABITAT.png" alt="faune" className="offers1 w3-block" />
                <div className="back w3-block w3-display-middle"> </div>
                <div className="w3-display-topleft w3-center w3-xxlarge w3-text-white holy w3-margin">
                    Picturesque based landscapes experiencing
                </div>
            </div>
            <div className="container">
                <div style={{ width: "100%" }} className="w3-margin-top">
                    <h1 className="offersTitle color">{Offers.title.Title}</h1>
                </div>
                <div className="row fauneAnFloreContainer" style={{ marginTop: 36 }}>
                    {offers.map(item => (item))}
                </div>
            </div>
        </div>
    )
}