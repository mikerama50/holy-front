const offers = {
    title:{
        Title:"Madagascar has an interesting geological formation as it was piece  broken apart from Gondwana mass land, each region has its own particular geological feature ranging:",
    },
    off:[
        {
            id:"",
            name:"tsingyBemaraha",
            titlePart1:"TSINGY OF",
            titlePart2:"BEMARAHA",
            description:"Bemaraha tsingy is composed of a block of limestone that formed at the bottom of the ocean during the ancient time. It is one of the largest and wonders protected area in Madagascar and has been recognized as a UNESCO Natural World heritage site.",
            image:"/image/gallery/tsingy de bemaraha.jpg"
        },
        {
            id:"",
            name:"redTsingy",
            titlePart1:"Red",
            titlePart2:"Tsingy",
            description:"Red tsingy is lateritic terrain cover most of the shield landscapes of Madagscar.They have been considered as chemical weathering of parent rock. This Tsingy like structures and textures is chiefly result of degradation and loss of most iron oxide related element.",
            image:"/image/gallery/tsingy rouge.png"
        },
        {
            id:"",
            name:"Isalo",
            titlePart1:"Isalo",
            titlePart2:"NATIONAL PARC",
            description:"The Isalo massif is well -known sandstone area in central-southern Madagascar, is one the most visited park, cause of the beauty of its landscape and by the spectacular canyons.The sandstone of Isalo massif is continental origin, which covers an area of 21 540 ha and home to different unique Xerophytic Rocky vegetation and Sclerophylous woodland and other kinds of plant community.",
            image:"/image/gallery/Isalo.png"
        }
    ]
}

export default offers;