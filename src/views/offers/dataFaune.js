const offers = {
    title:{
        Title:"Madagascar was one of the centers of diversity due to two major divisions of vegetation patterns. The eastern region consists of tropical humid forest whereas the western region is tropical dry forest, and wooded grassland mosaic throughout the center and some part of the west. Madagascar has different varieties of natural habitats where many kinds of fauna and flora have evolved differently. Those habitats include:",
    },
    off:[
        {
            id:"",
            name:"Spiny",
            titlePart1:"Spiny",
            titlePart2:"forest",
            description:"It is one of the few habitats found in the world; this kind of habitat is mainly located in the southern part of Madagascar and is highly seasonal environment. More than eighty percent of the plants of the spiny forest are locally endemic; and the most dominant species of plants include Euphorbia and members of DIDIERACEAE. Those plants have incredible form of adaptation in order to cope with unpredictable rainy season.",
            image:"/image/gallery/spiny forest2.png"
        },
        {
            id:"",
            name:"Tropical humid",
            titlePart1:"Tropical",
            titlePart2:"humid forest",
            description:"This forest is naturally located in the North East part of the island and through the center, and up to the southern. Tropical humid forest is the most divers in term of species, where majority of orchids and many other important faunal species inhabit.",
            image:"/image/gallery/FORET.png"
        },
        {
            id:"",
            name:"Tropical dry",
            titlePart1:"Tropical dry",
            titlePart2:"deciduous forest",
            description:"It is mainly seen along the west and southwest part of Madagascar which accommodates some endemic floral and faunal diversity. This is a typical habitat where wildlife watching is easy due to the flat trail and forest is less dense.",
            image:"/image/gallery/dry forest.jpg"
        },
        {
            id:"",
            name:"Wetland",
            titlePart1:"Wetland",
            titlePart2:"habitat forest",
            description:"Spread along the coastal area of Madagascar such as mangrove and littoral forest which provide important habitat both for marine organism and terrestrial species. ",
            image:"/image/gallery/wetland.jpg"
        },
        {
            id:"",
            name:"Savanna wooded",
            titlePart1:"Savanna",
            titlePart2:"wooded",
            description:"It is the result of human modification of the landscape, this habitat is the dominate vegetation in most central and west part of Madagascar.",
            image:"/image/gallery/SAVANNA GRASSLAND.png"
        }
    ]
}

export default offers;