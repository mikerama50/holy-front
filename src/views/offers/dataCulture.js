const offers = {
    title:{
        Title:"Historical traditions are important to all Malagasy groups, and material testimony of the past in form of typical villages erected stones and other cultural and social value. ",
    },
    off:[
        {
            id:"",
            name:"origin",
            titlePart1:"Malagasy",
            titlePart2:"Origin",
            description:"The Malagasy people have a rich cultural heritage from their Asian, Malayo-Polynesian, African and even European origin.That ancestral combination origin resulted in regional cultural and art diversity among the Malagasy ethnic group.",
            image:"/image/gallery/CULTURAL.JPG"
        },
        {
            id:"",
            name:"value",
            titlePart1:"Origin",
            titlePart2:"Value",
            description:"Like the central highland people are descended from Malayo- Polynesian origin, they have their most significant historical place in the central plateau where visitors informed the cultural and historical value through the combination of its physical, mental and social value.",
            image:"/image/gallery/ara malagasy.png"
        },
        {
            id:"",
            name:"oral",
            titlePart1:"Traditional",
            titlePart2:"literature",
            description:" Formal oratorical speeches (kabary), folk tales (angano), proverbs components also make the Malagasy language and unique that sparks interests among linguist and philosopher. Popular proverbs that shows the warmest Malagasy hospitality towards people “Ny sakafo masaka tsy misy tompony” the food which is prepared has no master.A holy destination informs the travelers with all those cultural values found across Madagascar based on their purchased product as bonus.",
            image:"/image/gallery/zafy.png"
        },  
    ]
}

export default offers;