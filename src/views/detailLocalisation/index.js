import React from "react";
import data from "../../data/dataLocalisation";
import dataImage from "../../data/dataDetails";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Timeline, TimelineEvent } from "react-event-timeline";
import "../../assets/css/detailLocalisation.css";
import "../../assets/css/welcome_page.css";
import $ from 'jquery';
import dataItinerary from "../../data/dataItinerary";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
const categories = ['lemur', 'flora', 'insect', 'landscape', 'reptile', 'handicraft', 'amphibian', 'geological', 'chameleon', 'bird', 'cultural', 'tortoise'];

export default class DetailLocalisation extends React.Component {

    state = {
        galleryContainer: [],
        itineraries: [],
        video: "",
        content: "",
        description: ""
    }

    ToggleViewMore = (event) => {
        const element = event.currentTarget;
        if (element.innerHTML === "View more photos") {
            element.innerHTML = "Hide";
        } else if (element.innerHTML === "Hide") {
            element.innerHTML = "View more photos"
        }
        element.previousElementSibling.classList.toggle("w3-hide");
    }

    builGalleryContent = () => {
        var k = 0;
        const localisation = this.props.match.params.name;
        var categoriesContent = [];
        for (const categorie of categories) {
            var dataCategorie = dataImage[categorie];
            var localisationsContent = [];
            for (var i = 0; i < dataCategorie.imagesByLocalisation.length; i++) {

                var dataSpecies = dataCategorie.imagesByLocalisation[i].species;
                var imageContent = [];
                var imageContentHide = [];
                if (dataCategorie.imagesByLocalisation[i].localisation === localisation) {
                    for (var j = 0; j < dataSpecies.length; j++) {
                        var kilasy = "";
                        if (dataSpecies[j].video) {
                            if (dataSpecies[j].video.length > 7) {
                                kilasy = "w3-display-bottomright w3-red w3-text-white ImageByLocVideo w3-round-xxlarge w3-hover-grey";
                            } else {
                                kilasy = "w3-display-bottomright w3-red w3-text-white ImageByLocVideo w3-round-xxlarge w3-hide";
                            }
                        }
                        if (dataSpecies.length < 17) {
                            imageContent.push(
                                <div key={j} className="w3-col m4 s6 l3 w3-margin-bottom allImageByLoc w3-display-container w3-round-xlarge">
                                    <div className="clickMyImage w3-display-middle w3-block w3-height">
                                        <LazyLoadImage src={dataSpecies[j].image} className="imageDetailLocalisation w3-block w3-height w3-round-xlarge" alt={dataSpecies[j].name} />
                                    </div>
                                    <div className="w3-display-topleft w3-round-xxlarge w3-teal w3-margin-bottom span"><b>{localisation.substr(0, 3).toUpperCase() + " " + (k = k + 1)}</b></div>
                                    <div className="w3-display-bottomright w3-grey w3-text-white ImageByLoc w3-round-xxlarge w3-large">Loc: {dataSpecies[j].loc}</div>
                                    <div id={k} className={kilasy + " clickMe"} title="Click to show video"><FontAwesomeIcon icon={faPlay} /></div>
                                    <div className="w3-hide" id={localisation.substr(0, 3).toUpperCase() + k}>{dataSpecies[j].video}</div>
                                </div>
                            )
                        } else {
                            if (j < 16) {
                                imageContent.push(
                                    <div key={j} className="w3-col m4 s6 l3 w3-margin-bottom allImageByLoc w3-display-container w3-round-xlarge">
                                        <div className="clickMyImage w3-display-middle w3-block w3-height">
                                            <LazyLoadImage src={dataSpecies[j].image} className="imageDetailLocalisation w3-block w3-height w3-round-xlarge" alt={dataSpecies[j].name} />
                                        </div>
                                        <div className="w3-display-topleft w3-round-xxlarge w3-teal w3-margin-bottom span"><b>{localisation.substr(0, 3).toUpperCase() + " " + (k = k + 1)}</b></div>
                                        <div className="w3-display-bottomright w3-grey w3-text-white ImageByLoc w3-round-xxlarge w3-large">Loc: {dataSpecies[j].loc}</div>
                                        <div id={k} className={kilasy + " clickMe"} title="Click to show video"><FontAwesomeIcon icon={faPlay} /></div>
                                        <div className="w3-hide" id={localisation.substr(0, 3).toUpperCase() + k}>{dataSpecies[j].video}</div>
                                    </div>
                                )
                            } else {
                                imageContentHide.push(
                                    <div key={j} className="w3-col m4 s6 l3 w3-margin-bottom allImageByLoc w3-display-container w3-round-xlarge">
                                        <div className="clickMyImage w3-display-middle w3-block w3-height">
                                            <LazyLoadImage src={dataSpecies[j].image} className="imageDetailLocalisation w3-block w3-height w3-round-xlarge" alt={dataSpecies[j].name} />
                                        </div>
                                        <div className="w3-display-topleft w3-round-xxlarge w3-teal w3-margin-bottom span"><b>{localisation.substr(0, 3).toUpperCase() + " " + (k = k + 1)}</b></div>
                                        <div className="w3-display-bottomright w3-grey w3-text-white ImageByLoc w3-round-xxlarge w3-large">Loc: {dataSpecies[j].loc}</div>
                                        <div id={k} className={kilasy + " clickMe"} title="Click to show video"><FontAwesomeIcon icon={faPlay} /></div>
                                        <div className="w3-hide" id={localisation.substr(0, 3).toUpperCase() + k}>{dataSpecies[j].video}</div>
                                    </div>
                                )
                            }
                        }

                    }
                }
                if (imageContent.length > 0) {
                    if (dataSpecies.length < 17) {
                        localisationsContent.push(
                            <div key={j}>
                                <div className="w3-row-padding sect div1">
                                    {imageContent.map(item => (item))}
                                </div>
                            </div>
                        )
                    } else {
                        localisationsContent.push(
                            <div key={i}>
                                <div className="w3-row-padding sect div1">
                                    {imageContent.map(item => (item))}
                                </div>

                                <div className="w3-row-padding sect div2 w3-hide">
                                    {imageContentHide.map(item => (item))}
                                </div>
                                <div onClick={(event) => { this.ToggleViewMore(event) }} className="hideMore w3-xlarge w3-text-white w3-teal w3-round-large w3-margin-left w3-margin-bottom w3-card w3-padding">View more photos</div>
                            </div>
                        )
                    }

                }
            }
            if (localisationsContent.length > 0) {
                categoriesContent.push(
                    <div key={categorie}>
                        <h2 className="JuneGull w3-text-red borderBottom w3-third w3-margin w3-xxlarge w3-center">
                            {(categorie === "cultural" ? "Various interesting aspect" : categorie)}
                        </h2>
                        {
                            localisationsContent.map(item => (item))
                        }
                    </div>
                )
            }
        }
        this.setState({
            galleryContainer: categoriesContent
        });

    }

    buildCircuit = () => {
        const localisation = this.props.match.params.name;
        var itineraryProposed = data[localisation];
        var itineraries = [];
        if (itineraryProposed) {
            for (const proposed of itineraryProposed.circuits) {
                if (itineraries.length > 1) {
                    this.setState({
                        content: "w3-row w3-light-grey"
                    })
                } else {
                    this.setState({
                        content: "w3-row w3-content w3-light-grey"
                    })
                }

                var circuits = dataItinerary[proposed];
                var nameCircuit = circuits.title;
                var dayContent = [];
                for (var i = 0; i < circuits.days.length; i++) {
                    var day = circuits.days[i];
                    var content = [];
                    for (var j = 0; j < day.steps.length; j++) {
                        content.push(
                            <div key={j}>
                                <h4 className="vertical-timeline-element-subtitle w3-text-teal">{day.steps[j].title}</h4>
                                <p className="w3-text-grey">{day.steps[j].content}</p>
                            </div>
                        )
                    }
                    dayContent.push(
                        <TimelineEvent key={i}
                            iconColor="#009688" title="" createdAt={<h3 className="vertical-timeline-element-title w3-text-red JuneGull w3-xlarge">{day.name}</h3>}>
                            {
                                content.map(item => (item))
                            }
                        </TimelineEvent>
                    )
                }
                itineraries.push(
                    <>
                        <h2 className="w3-text-grey JuneGull w3-center w3-margin w3-padding w3-card w3-white" style={{ height: 100, overflow: "hidden" }}>{nameCircuit}</h2>
                        <Timeline key={proposed} lineColor="#009688">
                            {
                                dayContent.map(item => (item))
                            }
                        </Timeline>
                    </>
                )
            }
        }
        this.setState({
            itineraries: itineraries
        })
    }

    componentWillMount() {
        const localisation = this.props.match.params.name;
        this.setState({
            description: data[localisation].description
        })
        $(document).ready(() => {
            $('.clickMe').click(function () {
                var idf = localisation.substr(0, 3).toUpperCase() + this.id;
                var url = document.getElementById(idf).innerHTML;

                if (url.length > 7) {
                    $("#videoContainer").attr({ "src": url });
                    $("#id01").css({ "display": "block" });
                } else {
                    alert("No video found, Comingsoon...");
                }
            });

            $('.close').click(function () {
                $("#videoContainer").attr({ "src": "" });
                $("#id01").css({ "display": "none" });
            });

            $('.clickMyImage').click(function () {
                $(".BigView").html(this.innerHTML);
                $("#id02").css({ "display": "block" });
            });

            setTimeout(() => {
                $(".flip-face").css({ "display": "none" });
                $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" });
                $(".flip-box-well").css({ "position": "static" });
            }, 1000)
        });
        this.builGalleryContent();
        this.buildCircuit();
    }

    render() {
        return (
            <>

                <div id="id01" className="w3-modal modal">
                    <div className="w3-modal-content  w3-round-xxlarge">
                        <div className="w3-container">
                            <span className="close w3-red w3-padding w3-margin w3-round-xxlarge w3-display-topright videoItinerary">&times;</span>
                            <div style={{ height: 500 }}>
                                <iframe src="" id="videoContainer" title="video" className="w3-round-xlarge w3-red w3-block videoItinerary w3-height" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="id02" className="w3-modal modal">
                    <div className="w3-modal-content  w3-round-xxlarge">
                        <div className="w3-container">
                            <span onClick={() => document.getElementById("id02").style.display = "none"} className="w3-red w3-padding w3-margin w3-round-xxlarge w3-display-topright videoItinerary">&times;</span>
                            <div style={{ height: 500 }} className="BigView w3-padding">

                            </div>
                        </div>
                    </div>
                </div>
                <a href="#targetItinerary">
                    <span className="w3-center w3-teal w3-text-white w3-large w3-round w3-card w3-padding grey pullItinerary w3-right">
                        <span className="buttonPull">Itinerary</span><br />
                        <FontAwesomeIcon icon={faAngleDoubleDown} />
                    </span>
                </a>

                <div className="flip-box-well w3-block">
                    <div className="w3-block w3-height w3-white w3-display-container flip-box-inner-well">
                        <div className="flip-face">
                        </div>
                        <div className="flip-box-back-well w3-center w3-block w3-height w3-white">
                            <h1 className="BugsLife w3-text-teal w3-center w3-jumbo loc w3-light-grey">{this.props.match.params.name}{
                                this.props.match.params.name === "Nosy Be" && <h2>Nosy Be Archipelago Northern Madagascar</h2>
                            }
                            </h1>
                            <div className="w3-content w3-panel w3-leftbar w3-border-red" style={{ marginTop: 36, marginBottom: 36 }}>
                                <p className="grey w3-large w3-margin-left">{this.state.description}</p>
                            </div>
                            <div className="galleryContainer">
                                {
                                    this.state.galleryContainer.map(item => (item))
                                }
                            </div>
                            <div id="targetItinerary" style={{ height: 90 }}></div>
                            <div className={this.state.content} id="">
                                <h1 className="w3-xxxlarge w3-text-red w3-center JuneGull w3-margin-top">our itinerary proposition</h1>
                                {
                                    this.state.itineraries.map((item, key) => (
                                        <div key={key} className={"w3-col l" + (12 / this.state.itineraries.length)}>{item}</div>
                                    ))
                                }
                            </div>
                            <div className="w3-row">
                                <h1 className="w3-xxxlarge w3-text-red w3-center JuneGull weCan w3-margin">we can elaborate an itinerary on your preference</h1>
                            </div>
                            <div className="w3-block w3-display-container" style={{ height: 100 }}>
                                <div className="w3-display-middle">
                                    <button onClick={() => window.location.assign("/contactUs")} className="btnContactUs">Contact us</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}