import React, { useRef, useState } from "react";
import JoditEditor from "jodit-react";
import Axios from "axios";
import "../../assets/css/contactUs.css";

const url = "http://localhost:3000";

export default function ContactUs(prop) {

    const editor = useRef()
    const [content, setContent] = useState('');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [subject, setSubject] = useState('');
    const [btnName, setBtnName] = useState('Submit')

    const config = {
        readonly: false, // all options from https://xdsoft.net/jodit/doc/
        height: 400,
        placeholder: "Write here your message ..."
    }

    const onSubmit = (e) => {
        e.preventDefault();
        setBtnName("loading...");
        Axios.post(url + "/send_mail", {
            from: email,
            subject: subject,
            message: content,
            firstname,
            lastname
        }).then(() => {
            window.location.assign("/home/noWelcome");
        }).catch(err => {
            alert(err.message);
        });
    }

    return (
        <div className="w3-content">
            <h1 className="w3-center titleContactUs">Contact Us</h1>
            <div className="w3-card w3-padding w3-round formContainer">
                <form onSubmit={(event) => onSubmit(event)}>
                    <div className="w3-row-padding w3-margin-top">
                        <div className="w3-col s12 m6 l6">
                            <label htmlFor="firstName">First name</label>
                            <input type="text" name="firstname" id="firstName" required className="inputContactUs" onChange={(event) => setFirstname(event.currentTarget.value)} />
                        </div>
                        <div className="w3-col s12 m6 l6">
                            <label htmlFor="Last name">Last name</label>
                            <input type="text" id="lastName" name="lastname" required className="inputContactUs" onChange={(event) => setLastName(event.currentTarget.value)} />
                        </div>
                    </div>
                    <div className="w3-row-padding w3-margin-top">
                        <div className="w3-col s12 m6 l6">
                            <label htmlFor="email">Email</label>
                            <input type="email" id="email" name="email" required className="inputContactUs" onChange={(event) => setEmail(event.currentTarget.value)} />
                        </div>
                        <div className="w3-col s12 m6 l6">
                            <label htmlFor="subject">Subject</label>
                            <input type="text" id="subject" name="subject" required className="inputContactUs" onChange={(event) => setSubject(event.currentTarget.value)} />
                        </div>
                    </div>
                    <div className="w3-row-padding w3-margin-top">
                        <div className="w3-col s12 m12 l12">
                            <label>Message</label>
                            <JoditEditor
                                ref={editor}
                                value={content}
                                config={config}
                                tabIndex={1}
                                onBlur={newContent => setContent(newContent.currentTarget.innerHTML)}
                            />
                        </div>
                    </div>
                    <div className="w3-display-container w3-block" style={{ height: 60, marginTop: 24, marginBottom: 24 }}>
                        <div className="w3-display-middle">
                            <button className="btnSubmit" type="submit">{btnName}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )

}