import React, { useState, useEffect } from "react";
import { Animated } from "react-animated-css";
import "../../assets/css/experience.css";
import "../../assets/css/animate.min.css";
import "animate.css";
import { AnimatedOnScroll } from "react-animated-css-onscroll";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Dennis from "../../assets/image/Dennis.png"
import Team from "../../assets/image/splash.jpg"
import "../../assets/css/welcome_page.css";
import $ from "jquery"
import { faShieldAlt, faGraduationCap, faCar, faCameraRetro, faPager, faTrophy} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCreativeCommonsRemix, faCreativeCommonsBy, faCreativeCommonsShare } from '@fortawesome/free-brands-svg-icons'

export default function () {
    const [anim, setAnim] = useState(
        <Animated className="w3-xxxlarge holy-tag" animationIn="fadeInDown" isVisible={true}>Holy Destination Madagascar</Animated>
    )

    useEffect(() => {

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationOut="fadeOutDown" isVisible={false}>Holy Destination Madagascar</Animated>
            )
        }, 2000);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationIn="fadeInDown" isVisible={true}>Est heureux de vous Accueillir</Animated>
            )
        }, 2500);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationOut="fadeOutDown" isVisible={false}>Est heureux de vous Accueillir</Animated>
            )
        }, 4500);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationIn="fadeInDown" isVisible={true}>Votre satisfaction est notre atout</Animated>
            )
        }, 5000);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationOut="fadeOutDown" isVisible={false}>Votre satisfaction est notre atout</Animated>
            )
        }, 7000);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationIn="fadeInDown" isVisible={true}>Votre reve sera effectivement exauccee</Animated>
            )
        }, 7500);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationOut="fadeOutDown" isVisible={false}>Votre reve sera effectivement exauccee</Animated>
            )
        }, 9500);

        setTimeout(() => {
            setAnim(
                <Animated className="w3-xxxlarge holy-tag" animationIn="fadeInDown" isVisible={true}>Holy Destination Madagascar</Animated>
            )
        }, 10000);

        $(document).ready(() => {
            setTimeout(() => {
                $(".flip-face").css({ "display": "none" });
                $(".flip-box-inner-well").css({ "transform": "rotateY(180deg)" });
                $(".flip-box-well").css({ "position": "static" });
            }, 1000)
        });
    }, [])
    return (
        <>
            <div class="flip-box-well w3-block">
                <div className="w3-block w3-height w3-white w3-display-container flip-box-inner-well">
                    <div className="flip-face w3-block w3-height w3-white">
                        
                    </div>

                    <div class="flip-box-back-well w3-center w3-block w3-height w3-white">
                        {/* <!-- Header --> */}
                        <div className="w3-display-container sigle">
                            <div className="back w3-block w3-display-middle"> </div>
                            <div className="w3-display-middle w3-center">
                                {anim}
                            </div>
                        </div>
                        <div className="container">

                            {/* <!-- !PAGE CONTENT! --> */}
                            <div className="w3-main">
                                {/* <!-- Services --> */}
                                <div className="w3-container">
                                    <div className="w3-display-container" style={{ marginTop: -50, marginBottom: 250 }}>
                                        <div className="w3-display-middle">
                                            <div className="imageContainer w3-white" style={{ marginTop: 150 }}>
                                                <FontAwesomeIcon icon={faTrophy} className="itineraryImage w3-text-amber w3-jumbo"/>
                                            </div>
                                        </div>
                                        <h4 className="w3-xlarge grey w3-display-middle w3-block w3-center" style={{ marginTop: 180 }}>A few words from HOLY DESTINATION's Team:</h4>
                                        <div className="w3-display-middle w3-block w3-transparent w3-container" style={{ height: 350 }}> </div>
                                    </div>
                                    <h2 className="w3-xxlarge amber"><FontAwesomeIcon icon={faPager} className="w3-xxxlarge" /><br></br>About us</h2>
                                    <hr style={{ width: 50, border: "5px solid rgb(255, 40, 40)", marginBottom: 50 }} className="w3-round" />
                                    <div className="w3-row-padding">
                                        <AnimatedOnScroll animationIn="slideInLeft" className="w3-col m12 l4 w3-margin-bottom">
                                            <div className="w3-light-grey myCard">
                                                <div className="w3-container">
                                                    <h3 className="red w3-center w3-margin"><FontAwesomeIcon icon={faCreativeCommonsRemix} className="w3-xxlarge" /><br></br>Creation</h3>
                                                    <p className="grey w3-large">
                                                        Holy Destination is an inbound Tour Operator based in Antananarivo Madagascar. It was created in 2008, and since then we have organized different tours across Madagascar.
                                                    </p>
                                                </div>
                                            </div>
                                        </AnimatedOnScroll>
                                        <AnimatedOnScroll animationIn="fadeIn" className="w3-col m12 l4 w3-margin-bottom">
                                            <div className="w3-light-grey myCard">
                                                <div className="w3-container">
                                                    <h3 className="red w3-center w3-margin"><FontAwesomeIcon icon={faCreativeCommonsBy} className="w3-xxlarge" /><br></br>Team</h3>
                                                    <p className="grey w3-large">
                                                        Holy Destination Tour Operator is made up of a very dynamic team, very united because we share the same feelings: the love of our very magnificent motherland Madagascar and its richness in biodiversity and culture.
                                                    </p>
                                                </div>
                                            </div>
                                        </AnimatedOnScroll>
                                        <AnimatedOnScroll animationIn="slideInRight" className="w3-col m12 l4 w3-margin-bottom" style={{ marginBottom: 50 }}>
                                            <div className="w3-light-grey myCard">
                                                <div className="w3-container">
                                                    <h3 className="red w3-center w3-margin"><FontAwesomeIcon icon={faCreativeCommonsShare} className="w3-xxlarge" /><br></br>Service</h3>
                                                    <p className="grey w3-large">
                                                        In order to respond as much as possible to the satisfaction of our customers, and in order to give them the happiness they are looking for by coming to explore and venture into the different natural habitats home to endemic fauna and flora species in our country.
                                        </p>
                                                </div>
                                            </div>
                                        </AnimatedOnScroll>
                                    </div>
                                    <h2 className="w3-xxlarge amber"><FontAwesomeIcon icon={faGraduationCap} className="w3-xxxlarge" /><br></br>Experience & Skills</h2>
                                    <hr style={{ width: 50, border: "5px solid rgb(255, 40, 40)" }} className="w3-round" />
                                    <AnimatedOnScroll animationIn="fadeInUp" className="w3-margin-left">
                                        <p className="grey w3-large">
                                            For 11 years, we have not stopped organizing tours to different amazing destinations on this paradise island for the happiness of tourists and foreigners who want to discover the delightful richness that do not exist elsewhere than here on our lovely Island. Depending on your proposal, we are delighted to concoct tailor-made tours, honeymoons, hikes, themed tours for individuals or groups.
                                                    </p>
                                        <p className="grey w3-large">We work closely with professional, naturalist and specialist guides. Our guides will make you discover all the aspects of the wild life developing in the various majestic terrestrial ecosystems of Madagascar including among others the vast humid tropical forests, the gallery forests as well as the spiny forests of the arid regions of Madagascar. These guides will also take you to stumble across all the selected and specific geological characteristics of certain regions of our multifaceted island such as the Isalo massif, the “inselberg” granite formations of the Ibity and Andringitra mountains, the Tsingy de Bemaraha which is an emblematic rock formation of Madagascar. Do not forget to discover the maritime richness of the island such as the seasonal whale watching, the largest beaches like the beach of Nosy be as well as the various related activities for water sports adepts (windsurfing, diving, kite surfing…).</p>
                                        <p className="grey w3-large">Concerning accommodations and various services to make your trip pleasant and unforgettable, Holy Destination works closely with various hotels and service providers who only seek to satisfy the wishes of customers.</p>
                                    </AnimatedOnScroll>
                                </div>

                                {/* <!-- Warranty --> */}
                                <div className="w3-container" style={{ marginTop: 50 }}>
                                    <h2 className="w3-xxlarge amber"><FontAwesomeIcon icon={faShieldAlt} className="w3-xxxlarge" /><br></br>Warranty & Security</h2>
                                    <hr style={{ width: 50, border: "5px solid red" }} className="w3-round" />
                                    <AnimatedOnScroll animationIn="fadeInUp" className="w3-margin-left">
                                        <p className="grey w3-large">Throughout your stay in Madagascar, we assure you a very strict follow-up of your itinerary. We give you the full guarantee that your itinerary will be carefully studied according to your request. Making your trip to Madagascar unforgettable and enjoyable is the wish we strive to fulfill.</p>
                                        <p className="grey w3-large">So entrust us with the organization of your trip, we will be very happy to give you complete satisfaction of your dream.</p>
                                    </AnimatedOnScroll>
                                </div>

                                {/* <!-- Transport --> */}
                                <div className="w3-container" style={{ marginTop: 50 }}>
                                    <h2 className="w3-xxlarge amber"><FontAwesomeIcon icon={faCar} className="w3-xxxlarge" /><br></br>Transport</h2>
                                    <hr style={{ width: 50, border: "5px solid red" }} className="w3-round" />
                                    <AnimatedOnScroll animationIn="fadeInUp" className="w3-margin-left">
                                        <p className="grey w3-large">In order to satisfy our customers, Holy Destination Tour Operator always strives to give the maximum.
                                For this, we have the means at our disposal so that your transport is as pleasant as possible and which can take you to destinations of your choice in Madagascar. We have a ship, 3 speedboats and 4x4 cars in excellent driving condition.</p>
                                        <p className="grey w3-large">But apart from that we work with providers who can help us accomplish this wonderful task of making your trip to Madagascar as pleasant as possible.</p>
                                    </AnimatedOnScroll>
                                </div>

                                {/* <!-- The Team --> */}
                                <div className="w3-row-padding" style={{ marginTop: 50 }}>
                                    <h2 className="w3-xxlarge amber"><FontAwesomeIcon icon={faCameraRetro} className="w3-xxxlarge" /><br></br>Team Pics</h2>
                                    <hr style={{ width: 50, border: "5px solid red" }} className="w3-round" />
                                    <AnimatedOnScroll animationIn="slideInLeft" className="w3-col l4 m4 w3-margin-bottom">
                                        <div className="w3-light-grey myCardTeam1 w3-display-container">
                                            <div className="w3-display-topmiddle imageContainerTeam w3-margin-top">
                                                <LazyLoadImage src={Dennis} className="itineraryImage" alt="Image" />
                                            </div>
                                            <div className="w3-display-topmiddle grey w3-block w3-padding w3-large" style={{ marginTop: 180, textAlign: 'justify' }}>Dennis RAKOTOSON:<br />I was born and educated in Madagascar, and has been leading groups of ecotravelers. Throughout the island for the past 15 years. I impart my wealth of knowledge of Madagascar's ecosystems in an easygoing and friendly manner. </div>
                                            <div className="w3-display-middle w3-block w3-transparent w3-container" style={{ height: 350, marginTop: -100 }}> </div>
                                        </div>
                                    </AnimatedOnScroll>
                                    <AnimatedOnScroll animationIn="slideInRight" className="w3-col l8 m8 w3-margin-bottom">
                                        <div className="w3-light-grey myCardTeam2 w3-display-container">
                                            <LazyLoadImage src={Team} className="teamImage w3-block w3-display-middle" alt="Image" />
                                            <div className="w3-display-middle w3-block w3-transparent w3-container" style={{ height: "100%" }}> </div>
                                        </div>
                                    </AnimatedOnScroll>
                                </div>
                                {/* <!-- End page content --> */}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}