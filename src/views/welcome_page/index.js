import React, { useEffect, useState } from "react";
import "../../assets/css/welcome_page.css";
import "../../assets/css/animate.min.css";
import SplashImage from "../../assets/image/splash.jpg";
import mada from "../../assets/image/mada1.png";
import logo from "../../assets/image/logo.png";
import lakana from "../../assets/image/lakana.png";
import { Animated } from "react-animated-css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import $ from "jquery";
import "animate.css";
import Gallery from "../gallery"


export default function (prop) {
    const [warm, setWarm] = useState(<Animated className="warm" animationIn="zoomIn" animationInDelay={2000}>We warmly welcome you to Madagascar</Animated>)

    useEffect(() => {
        $(document).ready(() => {

            setTimeout(() => {
                $(".welcomeMessageContainer").html("");
                $(".welcomeMessageContainer").append("<div class='welcome animate__animated animate__backOutRight'>Welcome to</div>")
                setWarm(<Animated className="warm" animationOut="fadeOut" isVisible={false}>We warmly welcome you to Madagascar</Animated>)
            }, 4500);
            setTimeout(() => {
                $(".welcomeMessageContainer").html("");
                $(".welcomeMessageContainer").append("<div class='welcome animate__animated animate__backInLeft'>Bienvenue a</div>")
                setWarm(<Animated className="warm" animationIn="fadeIn" isVisible={true}>Nous vous souhaitons la bienvenue à Madagascar!</Animated>)
            }, 5000);
            setTimeout(() => {
                $(".welcomeMessageContainer").html("");
                $(".welcomeMessageContainer").append("<div class='welcome animate__animated animate__backOutRight'>Bienvenue a</div>")
                setWarm(<Animated className="warm" animationOut="fadeOut" isVisible={false}>Nous vous souhaitons la bienvenue à Madagascar!</Animated>)
            }, 8000);
            setTimeout(() => {
                $(".welcomeMessageContainer").html("");
                $(".welcomeMessageContainer").append("<div class='welcome animate__animated animate__backInLeft'>Bienvenida a</div>")
                setWarm(<Animated className="warm" animationIn="fadeIn" isVisible={true}>¡Le damos una calurosa bienvenida a Madagascar!</Animated>)
            }, 8500);
        });
    }, []);

    return (
        <div class="flip-box-well w3-block">
            <div className="w3-block w3-height w3-display-container flip-box-inner-well">
                <div className="w3-containe welcomeContainer w3-block w3-teal w3-height flip-box-front-well">
                    <div className="w3-display-container w3-twothird w3-height w3-overflow-hidden">
                        <img src={SplashImage} alt="blur" className="w3-display-topright w3-block splashImage" />
                        <Animated className="w3-display-middle w3-block back" animationIn="fadeIn" animationInDelay={1000} isVisible={true}>
                            <div className="back"> </div>
                        </Animated>
                        <Animated className="logo-well w3-display-topmiddle" animationIn="fadeInUp" animationInDelay={2000} isVisible={true}>
                            <img src={logo} alt="blur" className="w3-block" />
                        </Animated>
                        <div className="w3-display-left welcomeCase w3-block">
                            <div className="topbar animate__animated animate__fadeInUp animate__delay-3s"></div>
                            <div className="welcome welcomeMessageContainer">
                                <div className="welcome animate__animated animate__backInUp animate__delay-1s">Welcome to</div>
                            </div>
                            <div className="holy w3-text-teal animate__animated animate__bounceInRight animate__delay-2s">Madagascar</div>
                            {warm}
                        </div>
                        <div className="w3-display-bottommiddle animate__animated animate__bounceIn animate__delay-4s" style={{ bottom: 10 }}>
                            <a href="/home/noWelcome">
                                <div className="skipButton">
                                    <FontAwesomeIcon icon={faAngleDoubleDown} />
                                </div>
                            </a>
                        </div>
                    </div>
                    <div className="w3-third w3-height">
                        <Animated className="w3-block w3-height w3-teal" animationIn="fadeIn" animationInDelay={1000} isVisible={true}>

                        </Animated>
                    </div>
                    <Animated className="mada w3-display-middle w3-container" animationIn="fadeIn" animationInDelay={1000} isVisible={true}>
                        <img src={mada} alt="blur" className="w3-block" />
                    </Animated>

                    {/* sambo */}

                    <Animated className="sambo2 w3-display-topright w3-container" animationIn="slideInRight" animationInDelay={3500} animationInDuration={2000} isVisible={true}>
                        <img src={lakana} alt="blur" className="w3-block sfd" />
                    </Animated>
                    <Animated className="sambo3 w3-display-bottomright w3-container" animationIn="slideInRight" animationInDelay={2000} animationInDuration={2000} isVisible={true}>
                        <img src={lakana} alt="blur" className="w3-block sfd" />
                    </Animated>

                </div>
                <div class="flip-box-back-well w3-center w3-block w3-height w3-white">
                    <Gallery />
                </div>
            </div>
        </div>
    )
}