import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//component declaration
const Home = React.lazy(() => import('./views/home/'));
const Itinerary = React.lazy(() => import("./views/itinerary"));
const PicturesqueHabitats = React.lazy(() => import("./views/offers/PicturesqueHabitat"));
const FauneFlore = React.lazy(() => import("./views/offers/FauneFlore"));
const Culture = React.lazy(() => import("./views/offers/Culture"));
const Detail = React.lazy(() => import("./views/detail"));
const ExperiencSecurity = React.lazy(() => import("./views/experience&warranty"));
const DetailLocation = React.lazy(() => import("./views/detailLocalisation"));
const GalleryVideo = React.lazy(() => import("./views/galleryVideo"));
const ContactUs = React.lazy(() => import("./views/contactUs"));

export default function router_manager() {
    return (
        <Suspense fallback={null}>
            <Router>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/home/noWelcome" exact component={Home} />

                    {/* itinerary */}
                    <Route path="/itinerary/:name" exact component={Itinerary} />

                    {/* offres */}
                    <Route path="/offers/PicturesqueHabitat" exact component={PicturesqueHabitats} />
                    <Route path="/offers/FauneFlore" exact component={FauneFlore} />
                    <Route path="/offers/AmazingCulture" exact component={Culture} />

                    {/* detail */}
                    <Route path="/detail/:categorie" exact component={Detail} />
                    <Route path="/detail/localisation/:name" exact component={DetailLocation} />
                    <Route path="/detail/localisation/:name/:species" exact component={DetailLocation} />

                    {/* experience & security */}
                    <Route path="/experience_warranty" exact component={ExperiencSecurity} />

                    {/* Gallery video */}
                    <Route path="/video" exact component={GalleryVideo} />

                    {/*contact us*/}
                    <Route path="/contactUs" exact component={ContactUs} />

                    <Route component={() => (<h1>Error 404 !!!</h1>)} />
                </Switch>
            </Router>
        </Suspense>
    );
} 